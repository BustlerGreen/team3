﻿using IdentityServer4.Models;

namespace IDServer
{
    public class IdentityConfiguration
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new  ApiResource("Organization")
                {
                    Scopes = new List<string>{"Organization.Read", "Organiztion.Write"},
                    ApiSecrets =  new List<Secret>{new Secret("orgsecret".Sha256()) },
                }
            };
        
        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("Organization.Read"),
                new ApiScope("Organization.Write"),
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "OrgBrowser",
                    ClientName = "Organization Browser",
                    ClientSecrets = {new Secret("orgorg".Sha256()) },
                    AllowedScopes = {"Organization.Read"},
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                }
            };

    }
}
