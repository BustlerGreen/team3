import { combineReducers } from "@reduxjs/toolkit";
import { USER_LOGIN, USER_LOGOUT, ADD_EMPLOYEE, EDIT_EMPLOYEE, userLogin, userLogout, addEmployee, editEmployee, END_EDIT_VACATION } from "./actions";
import { EDIT_VACATION, editVacation } from "../storage/actions";

const userData = {
    id: "",
    login: "",
    psw: "",
    jwtToken: "",
    fullName: "",
    orgId: "",
    vacToConfirm: [{
        fullName: "",
        edVacId: "",
        comment: "",
        dateStart: "",
        dateEnd: "",
        }
    ]

};

function user(state = userData, action){
    switch (action.type) {
        case USER_LOGIN : {
            return {...state, 
                id: action.payload.user.id, 
                login: action.payload.user.login, 
                psw: action.payload.user.psw, 
                jwtToken: action.payload.user.jwtToken,
                fullName: action.payload.user.fullName,
                orgId: action.payload.user.orgId,
                vacToConfirm: action.payload.user.vacToConfirm
            }
        }
        case USER_LOGOUT:{
            return {...state, 
                id: "", 
                login: "", 
                psw: "", 
                jwtToken: "",
                fullName: "",
                orgId: "",
                vacToConfirm: []
            }
        }
        default:{}
    }
    return state;
}
const vacData = {
    edVacId: "",
    comment: "",
    dateStart: "",
    dateEnd: "",
}
function vacation(state = vacData, action){
    switch(action.type){
        case EDIT_VACATION  :{
            return{ ...state,
               edVacId: action.payload.vacation.edVacId,
               comment: action.payload.vacation.comment,
               dateStart: action.payload.vacation.dateStart,
               dateEnd: action.payload.vacation.dateEnd,
            }
        }
        case END_EDIT_VACATION:{
            return{...state,
                edVacId: "",
                comment: "",
               dateStart: "",
               dateEnd: "",
            }
        }
        default:{}
    }
    return state;
}

export const reducer = combineReducers({user, vacation});