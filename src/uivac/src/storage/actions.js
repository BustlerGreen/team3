const USER_LOGIN = "USER_LOGIN";
const USER_LOGOUT = "USER_LOGOUT"
const ADD_EMPLOYEE = "ADD_EMPLOYEE";
const EDIT_EMPLOYEE = "EDIT_EMPLOYEE";

const EDIT_VACATION = "EDIT_VACATION";
const END_EDIT_VACATION = "END_EDIT_VACATION";

const userLogin = function(user) {return {type: USER_LOGIN, payload: {user},}}
const userLogout = function() {return {type: USER_LOGOUT, payload: {},}}


const addEmployee = function(employee){return{type: ADD_EMPLOYEE, payload: {employee},}}
const editEmployee = function(employee){return{type: EDIT_EMPLOYEE, payload: {employee},}}

const editVacation = function(vacation){return{type: EDIT_VACATION, payload: {vacation},}}
const endEditVacation = function(vacation){return{type: END_EDIT_VACATION, payload: {},}}

export {USER_LOGIN, USER_LOGOUT, ADD_EMPLOYEE,EDIT_EMPLOYEE, userLogin, userLogout, addEmployee, editEmployee};
export {EDIT_VACATION, editVacation, END_EDIT_VACATION, endEditVacation};