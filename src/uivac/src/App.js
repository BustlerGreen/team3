import './App.css';
import clsx from "clsx";
import {Routes, Route, useNavigate} from 'react-router-dom';
import Login from './components/login';
import React from 'react';
import { CssBaseline, AppBar, Toolbar, IconButton, Drawer, List, ListItem, ListItemText, Typography, Divider, Menu, MenuItem,Button,Box } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import HomeIcon from "@mui/icons-material/Home";
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import {styled, useTheme} from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux/es/exports';
import { SESSION_STOR_USER_KEY } from './commondef';
import { userLogin, userLogout } from './storage/actions';
import Body from './components/body';

const drawerWidth = 240;

const SAppBar = styled(AppBar, {
  shouldForwardProp: (prop) => prop !== 'openDrawer',
})(({ theme, openDrawer }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(openDrawer && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'openDrawer' })(
  ({ theme, openDrawer }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(openDrawer && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'space-between',
}));


export default function App() {
  const theme = useTheme()
  const [openDrawer, setOpenDrawer] = React.useState(false);
  const handleDrawerOpen = () => { setOpenDrawer(true) };
  const handleDrawerClose = () =>{ setOpenDrawer(false) };
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const handleMenuClose = () => { setAnchorEl(null); };

  const dispatch = useDispatch();
  const userData = useSelector( state => state.user);

  const navigate = useNavigate();
  
 /* React.useEffect(() => {
    try{
    const lssUserData = sessionStorage.getItem(SESSION_STOR_USER_KEY);
    if(lssUserData) {
      const userData = JSON.parse(lssUserData);
      dispatch(userLogin(userData));
    }
    return;
  }
  catch(err){}
  }, []);*/

  const account = userData.jwtToken ? userData.fullName ? userData.fullName : userData.login  : "Login";
  const logout = () => { 
    handleMenuClose(); 
    dispatch(userLogout());
    /*const url = `${CommonDef.backServer}/api/v1/Auth/logoff`;
    fetch(url, {
      method: "POST",
      mode: "cors",
      body: JSON.stringify({ticket: userData.id}),
      headers: { "Content-Type": "application/json", },
    })
    .then(res => res.json())
    .then(res => {
      dispath(usrLogout());*/
      sessionStorage.removeItem(SESSION_STOR_USER_KEY);
      navigate("/");
//    });
  };

  const handleAccountMenuOpen = event => { 
    if(!userData.jwtToken) { navigate('/login') ; return };
    setAnchorEl(event.currentTarget);
  };
  
  
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id="popUp menu"
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={_=>{}}>Профиль</MenuItem>
      <Divider />
      <MenuItem onClick={logout}>Выйти</MenuItem>
    </Menu>
  );

  const renderAppBar =(
      <SAppBar position="fixed" open={openDrawer}>
        <Toolbar variant="dense">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            sx={{ mr: 2, ...(openDrawer && { display: 'none' }) }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{flexGrow:1}}>team3.VacationPlanning</Typography>
            <Button  color="inherit"  aria-label="user account" aria-haspopup="true"
              onClick={handleAccountMenuOpen}>{account}</Button>
        </Toolbar>
      </SAppBar>
    );

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      {renderAppBar}
      {renderMenu}
      <Drawer variant='persistent' anchor='left' open={openDrawer}  
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
      >
        <DrawerHeader>
          <IconButton onClick={ _ => {navigate("/"); handleDrawerClose();}} >
            <HomeIcon/>
          </IconButton>
          <IconButton  onClick={handleDrawerClose} >
             {theme.direction === "ltr" ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          <ListItem button key={0}   
            onClick={() => { navigate("/vacations"); handleDrawerClose();}} >
            <ListItemText primary={<Typography  >мой отпуск</Typography>} />
          </ListItem>
          <ListItem button key={1}
            onClick={() =>{ navigate("/vacClaim"); handleDrawerClose();}}>
            <ListItemText primary={<Typography >Оформить отпуск</Typography>}/>  
          </ListItem>
          {(userData.vacToConfirm.length>0)&&( 
          <ListItem button key={2}
            onClick={() =>{ navigate("/vacConfirm"); handleDrawerClose();}}>
            <ListItemText primary={<Typography >Заявки на отпуск</Typography>}/>  
          </ListItem>)
          }
        </List>
      </Drawer>
      <Main open={openDrawer}>
        <DrawerHeader/>
        <Body/>
      </Main>
    </Box>
  );
}
