import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, IconButton, Table, TableBody, TableCell, TableHead, TableRow, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { formatDate, gcHostUrl } from "../commondef";
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import { EDIT_VACATION, editVacation, userLogin } from "../storage/actions";
import { useNavigate } from "react-router-dom";



export default function VacToConfirm() {
const dispatch = useDispatch();
const userData = useSelector(state=> state.user);

//const vacData = useSelector(state=> state.vacation);
const navigate =useNavigate();
const[vacs, setVacsState] = React.useState({
    vacRec: [{
        id: "",
        comment: "",
        dateStart: "",
        dateEnd: "",
        confirm: 0,
        mailSend: 0,
    }],
    showDeleteDlg: false,
    recToDel:"",
})

const Confirm = (id) => {
    fetch(`${gcHostUrl}/api/v1/Vacations/ConfirmVacationAsync/?VacationId=${id}`, {
        method: "PUT",
        mode: "cors",
        headers: {
            "Content-Type": "application/json",
        },
    })
    .then(responce => {
        const nudVac = userData.vacToConfirm.filter(item=>item.edVacId !== id);
        console.log(nudVac);
        userData.vacToConfirm = nudVac;
        dispatch(userLogin(userData));
    })
    .catch(err=>{
        setVacsState(oldState=>{return {
            ...oldState, recToDel: "", showDeleteDlg: false};
        })
    })
};



return(
        <Box sx={{display: "flex"}}>
            <Grid container directions="column">
                <Grid container margin="20px" justifyContent="center">
                    <Typography variant="h5">Активные заявки на отпуск</Typography>
                </Grid>
            <Grid container>
                <Table stickyHeader size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell style={{width:"2%"}}></TableCell>
                            <TableCell style={{width:"30%"}}>сотрудник</TableCell>
                            <TableCell style={{width:"20%"}}>начало</TableCell>
                            <TableCell style={{width:"20%"}}>окончание</TableCell>
                            <TableCell style={{width:"23%"}}>коментарий</TableCell>
                            <TableCell style={{width:"5%"}}>Утвердить</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(userData.vacToConfirm.length>0) && userData.vacToConfirm.map((item, idx) =>(
                            <TableRow key={idx}>
                                <TableCell >{idx+1}</TableCell> 
                                <TableCell >{item.fullName}</TableCell> 
                                <TableCell >{formatDate(new Date(item.dateStart))}</TableCell> 
                                <TableCell >{formatDate(new Date(item.dateEnd))}</TableCell> 
                                <TableCell >{item.comment}</TableCell> 
                                <TableCell  style={{padding: 0,}}>
                                    <IconButton color="primary" size="small" style={{display:item.confirm?"none":""}} onClick={()=>Confirm(item.edVacId)}>
                                        <ThumbUpOffAltIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow> 
                        ))
                        }
                    </TableBody>
                </Table>
            </Grid>
            </Grid>
        </Box>
    )
};