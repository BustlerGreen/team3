import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, IconButton, Table, TableBody, TableCell, TableHead, TableRow, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { formatDate, gcHostUrl } from "../commondef";
import EditIcon from '@mui/icons-material/Edit';
import { EDIT_VACATION, editVacation } from "../storage/actions";
import { useNavigate } from "react-router-dom";
import DeleteForeverIcon from '@mui/icons-material/Delete';


export default function Vacation() {
const dispatch = useDispatch();
const userData = useSelector(state=> state.user);
const vacData = useSelector(state=> state.vacation);
const navigate =useNavigate();
const[vacs, setVacsState] = React.useState({
    vacRec: [{
        id: "",
        comment: "",
        dateStart: "",
        dateEnd: "",
        confirm: 0,
        mailSend: 0,
    }],
    showDeleteDlg: false,
    recToDel:"",
})

const getVacations = () => {

    const cd = new Date().getFullYear().toString();
    fetch(`${gcHostUrl}/api/v1/Vacations/GetVacationByEmployeID/?EmployeId=${userData.id}&year=${cd}`, {
        method: "get",
        mode: "cors"
    })
    .then(res=>res.json())
    .then(responce => {
        const mVacs= responce.map(x=> {return {
            id: x.id,
            comment: x.comment,
            dateEnd: x.dateEnd,
            dateStart: x.dateStart,
            confirm: x.confirm,
            mailSend: x.mailSend
        }});
        setVacsState(oldState => {return{...oldState, vacRec: mVacs}});
        
    })
}
useEffect(getVacations,[vacs]);

const editRec= (vac) =>{
    const vacation = {edVacId: vac.id, dateEnd: vac.dateEnd, dateStart: vac.dateStart, comment: vac.comment};
    dispatch(editVacation(vacation));
    navigate("/vacClaim");
};

const deleteRec = () => {
    fetch(`${gcHostUrl}/api/v1/Vacations/DeleteVacationByID/?VacationId=${vacs.recToDel}`, {
        method: "DELETE",
        mode: "cors",

        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": `Bearer ${userData.jwtToken}`
        },
    })
    .then(responce => {
        const nvacRec = vacs.map(item=>item.id !== vacs.recToDel);
        setVacsState(oldState=>{return {
            ...oldState, vacRec: nvacRec, recToDel: "", showDeleteDlg: false};
        })
    })
    .catch(err=>{
        setVacsState(oldState=>{return {
            ...oldState, recToDel: "", showDeleteDlg: false};
        })
    })
};

const onDeleteRec = id => {
    setVacsState(oldState => {return{...oldState, recToDel: id, showDeleteDlg:true}})
};
const ConfirmDeleteDlg =(
    <React.Fragment>
      <Dialog open area-labelledby="confirm-delete">
        <DialogTitle>Подтвердите действие</DialogTitle>
        <DialogContent>
          <DialogContentText color="primary">Удалить выбранные записи?</DialogContentText>
          <DialogActions>
            <Button variant="outlined" onClick={e=>setVacsState(oldState => {return{...oldState, showDeleteDlg: false,}})}>Отмена</Button>
            <Button variant="outlined" onClick={deleteRec}>Ок</Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );

return(
        <Box sx={{display: "flex"}}>
            {vacs.showDeleteDlg&&ConfirmDeleteDlg}
            <Grid container directions="column">
                <Grid container margin="20px" justifyContent="center">
                    <Typography variant="h5">Статус заявок на отпуск</Typography>
                </Grid>
            <Grid container>
                <Table stickyHeader size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell style={{width:"2%"}}></TableCell>
                            <TableCell style={{width:"32%"}}>Коментарий</TableCell>
                            <TableCell style={{width:"25%"}}>начало</TableCell>
                            <TableCell style={{width:"25%"}}>окончание</TableCell>
                            <TableCell style={{width:"3%"}}>подтвержден</TableCell>
                            <TableCell style={{width:"3%"}}>уведомление</TableCell>
                            <TableCell style={{width:"5%"}}></TableCell>
                            <TableCell style={{width:"5%"}}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {vacs.vacRec && vacs.vacRec.map((item, idx) =>(
                            <TableRow key={idx}>
                                <TableCell >{idx+1}</TableCell> 
                                <TableCell >{item.comment}</TableCell> 
                                <TableCell >{formatDate(new Date(item.dateStart))}</TableCell> 
                                <TableCell >{formatDate(new Date(item.dateEnd))}</TableCell> 
                                <TableCell >{item.confirm}</TableCell> 
                                <TableCell >{item.mailSend}</TableCell> 
                                <TableCell  style={{padding: 0,}}>
                                    <IconButton color="primary" size="small" style={{display:item.confirm?"none":""}} onClick={()=>editRec(item)}>
                                        <EditIcon/>
                                    </IconButton>
                                </TableCell>
                                <TableCell  style={{padding:0,}}>
                                    <IconButton color="primary" size="small" style={{display:item.confirm?"none":""}} onClick={()=>onDeleteRec(item.id)}>
                                    <DeleteForeverIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow> 
                        ))
                        }
                    </TableBody>
                </Table>
            </Grid>
            </Grid>
        </Box>
    )
};