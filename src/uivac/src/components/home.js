import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import { Grid, Paper } from "@mui/material";
import image from "../resources/vac.PNG"


const styles = {
        height: "80vh",
    minWidth: 100,
    backgroundImage: `url(${image})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    backgroundSize: "contain",
    };
export default function Home() {    
        
    return (
        <React.Fragment>
          <Grid item>
            <Paper style={styles}/>
          </Grid>
        </React.Fragment>
    )
}