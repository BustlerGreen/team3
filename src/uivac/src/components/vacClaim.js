import { Box, Button, Grid, TextField, Typography } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { useNavigate } from "react-router-dom";
import { gcHostUrl } from "../commondef";
import { endEditVacation } from "../storage/actions";


export default function CreateVacClaim() {
    const dispatch = useDispatch();
    const userData = useSelector(state=>state.user);
    const vacData = useSelector(state=>state.vacation);
    const navigate = useNavigate();
    const [state, setState] = useState({
        fromDT: new Date(),
        toDT: new Date(),
        editMode: 0,
        comment: "",
    })

    useEffect(()=>{
        if(vacData.edVacId) setState(oldState=>{return{...oldState, fromDT: vacData.dateStart, toDT: vacData.dateEnd, comment: vacData.comment}})
    }, [vacData])
    
    const onChangeDT = (sel, dt) =>{
      if(sel === 1) setState(oldState =>{return{...oldState, fromDT: dt}});
      if(sel === 2) setState(oldState =>{return{...oldState, toDT: dt}});
    }
    const CreateVacClaim = () => {
        vacData.edVacId && fetch(`${gcHostUrl}/api/v1/Vacations/?id=${vacData.edVacId}`,{
            method: "PUT",
            mode: "cors",
            body: JSON.stringify({EmployeeId: userData.id, DateStart: state.fromDT, DateEnd: state.toDT, Comment: state.comment,
                VacationTypeId: "93729686-a369-4eeb-8bfa-cc69b6051d03", Confirm: 0, MailSend: 0}),
            headers: {
                "Content-Type" : "application/json",
            },
        })
        //.then(res=>res.json())
        .then(responce => {
            dispatch(endEditVacation());
            navigate("/vacations")
            return;
        })
        .catch(err=>{
            //console.log(err);
            return;
        });

        !vacData.edVacId &&fetch(`${gcHostUrl}/api/v1/Vacations`,{
            method: "post",
            mode: "cors",
            body: JSON.stringify({EmployeeId: userData.id, DateStart: state.fromDT, DateEnd: state.toDT, Comment: state.comment,
                VacationTypeId: "93729686-a369-4eeb-8bfa-cc69b6051d03", Confirm: 0, MailSend: 0}),
            headers: {
                "Content-Type" : "application/json",
            },
        })
        //.then(res=>res.json())
        .then(responce => {
            navigate("/vacations")
        })
        .catch(err=>{
            //console.log(err);
        });
    }
   
    return(
        <Box sx={{display: "flex"}}>
          <Grid container directions="column"  >
            <Grid container justifyContent="center"  >
                <Grid item margin="20px" >
                    <Typography variant="h4">{vacData.edVacId ? "Изменение заявки на отпуск":"Создание заявки на отпуск"}</Typography>
                </Grid>
            </Grid>
            <Grid container margin="20px" justifyContent="center">
                <TextField label="комментарий" value={state.comment} onChange={e=>setState(oldState=>{return{...oldState, comment: e.target.value}})}/>
            </Grid>
            <Grid container item justifyContent="space-evenly"  directions="vertical" >
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DatePicker name="fromDT" label="начало отпуска" ampm={false} value={state.fromDT} onChange={date => onChangeDT(1, date)} 
                        inputFormat="dd mmm yyyy" cancelLabel="Отмена" renderInput={(params)=> <TextField{...params}/>}/>
                    <DatePicker name="toDT" label="окончание отпуска" ampm={false} value={state.toDT} onChange={date => onChangeDT(2, date)} 
                        inputFormat="dd mmm yyyy" cancelLabel="Отмена" renderInput={(params)=> <TextField{...params}/>}/>
                </LocalizationProvider>
          </Grid>
          <Grid container item justify="center" justifyContent="center" margin="20px">
          <Button variant="outlined" color="primary" default
             style={{marginLeft:20, marginRight:20}} onClick={() => {dispatch(endEditVacation());vacData.edVacId? navigate("/vacations") : navigate("/")}}>Назад</Button>
          <Button variant="outlined" color="primary" 
            style={{marginLeft:20, marginRight:20}} onClick={() => CreateVacClaim()}>{vacData.edVacId?"Сохранить":"Оформить"}</Button>
          </Grid>
          </Grid>
        </Box>
    )
}