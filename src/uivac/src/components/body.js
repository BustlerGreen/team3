import React from "react";
import Login from "./login";
import { Routes, Route } from "react-router-dom";
import Home from "./home";
import Vacation from "./vacation";
import CreateVacClaim from "./vacClaim";
import VacToConfirm from "./vacToConfirm";

export default function Body() {
    return(
        <React.Fragment>
            <Routes>
                <Route exact path="/" element={<Home/>} />
                <Route path="/login" element={<Login/>} />
                <Route path="/vacations" element={<Vacation/>} />
                <Route path="/vacClaim" element={<CreateVacClaim/>} />
                <Route path="/vacConfirm" element={<VacToConfirm/>} />
            </Routes>
        </React.Fragment>
    )
}