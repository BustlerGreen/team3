import React, { useState } from 'react';
import { Grid, Paper, Typography, TextField, Button } from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { gcHostUrl } from '../commondef';
import { userLogin } from '../storage/actions';
import { SESSION_STOR_USER_KEY } from '../commondef';

export default function Login() {
    const navigate = useNavigate();
    const userData = useSelector(state=>state.user);
    const [state, setState] = useState({
        login: '',
        psw: '',
        loginerror:'',
        res: '',

    });
    const dispatch=useDispatch();

    const OnOk = ()=>{
        let user = {};
        fetch(`${gcHostUrl}/api/v1/Auth`,{
            method: "post",
            mode: "cors",
            body: JSON.stringify({Login: state.login, Password: state.psw}),
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(res=>{
            if(res.status !== 200) throw (new Error(`server responce status ${res.status}`));
            return res.json();
        })
        .then(responce=>{
            if(responce.error) throw (new Error("неверный логин/пароль"));
            setState(oldState => {return{...oldState, loginerror: "",}})
            user = {login: state.login, psw: state.psw, jwtToken: responce.token, id: responce.id, fullName: responce.fullName, orgId: responce.orgId, vacToConfirm: []}
            dispatch(userLogin(user));
            /*try{
                const serUsrData = JSON.stringify(user);
                sessionStorage.setItem(SESSION_STOR_USER_KEY, serUsrData);
              }
              catch(err){}*/
            
            //navigate("/");
        })
        .then (() =>{
            if(!user.orgId) {navigate("/"); return;}
            fetch(`${gcHostUrl}/api/v1/Vacations/GetVacationToConfirm/?orgId=${user.orgId}`,{
            method: "get",
            mode: "cors",
        })
        .then(res=>res.json())
        .then(responce=>{
            user.vacToConfirm = responce.map(x=> {return{edVacId: x.vacId, fullName: x.fullName, dateStart: x.dateStart, dateEnd: x.dateEnd, comment: x.comment}})
            dispatch(userLogin(user));
            console.log(userData)
            navigate("/");
        })})
        .catch(err=> {
            setState(oldState=>{return{...oldState, loginerror: JSON.stringify(err)}})
        })
        
        
        
    }

    const OnCancel = ()=>{
        navigate("/");
    }

    return (
        <React.Fragment>
            <Grid container justifyContent='center' marginTop="20px" >
                <Paper >
                    <Paper>
                        <Typography variant="h2">Login</Typography>
                    </Paper>
                    <Paper>
                        <Grid container justifyContent="center" direction="column" marginTop="20px">
                            <TextField label="логин" required color="secondary"  value={state.login}
                                onChange={e=> setState(oldState=> {return{...oldState, login: e.target.value,}})}/>
                            <TextField label="пароль" type="password" required color="secondary" value={state.psw}
                                onChange={e=> setState(oldState=> {return{...oldState, psw: e.target.value,}})}/>
                        </Grid>
                    </Paper>
                    <Paper >
                        <Grid container justifyContent="space-between" marginTop="20px" padding="10px">
                        <Button margin-right="20px" variant="contained" onClick={OnOk}>Login</Button>
                        <Button variant="contained" onClick={OnCancel}>Cancel</Button>
                        </Grid>
                    </Paper>
                    <Paper >
                    <Typography variant="h2">{state.loginerror}</Typography>
                    </Paper>
                </Paper>
            </Grid>
        </React.Fragment>
    )
}