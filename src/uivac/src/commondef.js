
export const gcHostUrl ="http://localhost:5228";

export const SESSION_STOR_USER_KEY = "team3.vac.ui.user.data";

export function formatDateTime(date){
    return `${(date.getDate()<10?"0":"")+date.getDate()}-${((date.getMonth()+1)<10?"0":"") +(date.getMonth()+1)}-${date.getFullYear()} ${(date.getHours()<10?"0":"") + date.getHours()}:${(date.getMinutes()<10?"0":"") + date.getMinutes()}`;
  }
  export function formatDate(date){
    return `${(date.getDate()<10?"0":"")+date.getDate()}-${((date.getMonth()+1)<10?"0":"") +(date.getMonth()+1)}-${date.getFullYear()} `;
  }