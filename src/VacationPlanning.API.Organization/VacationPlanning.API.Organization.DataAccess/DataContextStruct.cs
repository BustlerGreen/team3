using VacationPlanning.API.Organization.Core.Entities;

using VacationPlanning.API.Organization.DataAccess.ModelConfiguration;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace VacationPlanning.API.Organization.DataAccess;

public class DataContextStruct : DbContext
{
    public DbSet<OrgStructure> OrgStructures { get; set; }
    private IConfiguration _config;

    public  DataContextStruct(DbContextOptions<DataContextStruct> options, IConfiguration config)
        :base(options)
    {
        //Database.EnsureDeleted();
        //Database.EnsureCreated();
        _config = config;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(_config.GetConnectionString("Postgres"));
        // for migration
        //optionsBuilder.UseNpgsql("Host=localhost;Port=5433;Database=PgDbOrg;Username=vacplan;Password=vacplan");
    
       
    }
   //ДОбавление начальных данных по оргутсруктуре
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //modelBuilder.ApplyConfiguration(new OrgStructureConfiguration());
        modelBuilder.Entity<OrgStructure>().HasData(
            new OrgStructure()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Руководитель",
                //ParentId = null,
                IsFolder = 1

            },
            new OrgStructure()
            {
                Id = Guid.Parse("63729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Начальник IT",
                ParentId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 1
            },
            new OrgStructure()
            {
                Id = Guid.Parse("68729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Программист",
                ParentId = Guid.Parse("63729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 2
            },
            new OrgStructure()
            {
                Id = Guid.Parse("73729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Начальник ChR",
                ParentId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 1
            },
            new OrgStructure()
            {
                Id = Guid.Parse("73739686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Сотрудник ChR",
                ParentId = Guid.Parse("73729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 2
            }

        );
        //Добавление юр лица
        modelBuilder.Entity<LegalEntity>().HasData(
            new LegalEntity()
            {
                Id = Guid.Parse("11729686-a368-4eeb-8bfa-cc69b6050d02"),
                LegalEntityName = "Рога и копыта",
                EmployeID   = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d03")


            }
            );
    }

    //=====================================================================================================
    public class AppDbContextFactory : IDesignTimeDbContextFactory<DataContextStruct>
    {
        public DataContextStruct CreateDbContext(string[] args)
        {
            var OptionsBuilder = new DbContextOptionsBuilder<DataContextStruct>();
            OptionsBuilder.UseNpgsql("Host=localhost;Port=5433;Database=PgDbOrg;Username=vacplan;Password=vacplan");
            return new DataContextStruct(OptionsBuilder.Options, null);
        }
    }

}