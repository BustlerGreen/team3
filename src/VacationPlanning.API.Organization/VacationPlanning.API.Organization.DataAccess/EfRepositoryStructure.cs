using VacationPlanning.API.Organization.Core;
using VacationPlanning.API.Organization.Core.Entities;
using VacationPlanning.API.Organization.Core.Interfaces;

using VacationPlanning.API.Organization.DataAccess;

using Microsoft.EntityFrameworkCore;

namespace VacationPlanning.API.Organization.DataAccess
{
    public class EfRepositoryStructure<T>: IRepositoryStructure<T> 
        where T:BaseEntity
    {
        private readonly DataContextStruct _dataContext;
        public EfRepositoryStructure(DataContextStruct dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<T> CreateAsync(T item)
        {
            _dataContext.Add(item);
            _dataContext.SaveChanges();
            return Task.FromResult( item);
          
        }

        public Task<T> UpdateAsync(T entity)
        {
            _dataContext.Update(entity);
            _dataContext.SaveChanges();
            return Task.FromResult( entity);
        }

        public async Task<T> DeleteAsync(T item)
        {
            _dataContext.Remove(item);
            _dataContext.SaveChanges();
            return await Task.FromResult(item);
        }
        
    }
}