﻿
namespace VacationPlanning.API.Organization.DataAccess
{
    public class LegalEntity
    {
        public LegalEntity()
        {
        }

        public Guid Id { get; set; }
        public string LegalEntityName { get; set; }
        public Guid EmployeID { get; set; }
    }
}