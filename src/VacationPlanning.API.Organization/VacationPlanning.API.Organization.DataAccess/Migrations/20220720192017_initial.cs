﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VacationPlanning.API.Organization.DataAccess.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OrgStructures",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PositionName = table.Column<string>(type: "text", nullable: false),
                    ParentId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsFolder = table.Column<int>(type: "integer", nullable: false),
                    OrgStructureId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgStructures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrgStructures_OrgStructures_OrgStructureId",
                        column: x => x.OrgStructureId,
                        principalTable: "OrgStructures",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "OrgStructures",
                columns: new[] { "Id", "IsFolder", "OrgStructureId", "ParentId", "PositionName" },
                values: new object[,]
                {
                    { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), 1, null, new Guid("00000000-0000-0000-0000-000000000000"), "Руководитель" },
                    { new Guid("63729686-a368-4eeb-8bfa-cc69b6050d02"), 1, null, new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Начальник IT" },
                    { new Guid("68729686-a368-4eeb-8bfa-cc69b6050d02"), 2, null, new Guid("63729686-a368-4eeb-8bfa-cc69b6050d02"), "Программист" },
                    { new Guid("73729686-a368-4eeb-8bfa-cc69b6050d02"), 1, null, new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Начальник ChR" },
                    { new Guid("73739686-a368-4eeb-8bfa-cc69b6050d02"), 2, null, new Guid("73729686-a368-4eeb-8bfa-cc69b6050d02"), "Сотрудник ChR" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrgStructures_OrgStructureId",
                table: "OrgStructures",
                column: "OrgStructureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrgStructures");
        }
    }
}
