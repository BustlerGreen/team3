#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR "/src"
COPY ["VacationPlanning.API.Organization.WebHost/VacationPlanning.API.Organization.WebHost.csproj", "VacationPlanning.API.Organization.WebHost/"]
COPY ["VacationPlanning.API.Organization.Core/VacationPlanning.API.Organization.Core.csproj", "VacationPlanning.API.Organization.Core/"]
COPY ["VacationPlanning.API.Organization.DataAccess/VacationPlanning.API.Organization.DataAccess.csproj", "VacationPlanning.API.Organization.DataAccess/"]
RUN dotnet restore "VacationPlanning.API.Organization.WebHost/VacationPlanning.API.Organization.WebHost.csproj"
COPY . .
WORKDIR "/src/VacationPlanning.API.Organization.WebHost"
RUN dotnet build "VacationPlanning.API.Organization.WebHost.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "VacationPlanning.API.Organization.WebHost.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "VacationPlanning.API.Organization.WebHost.dll"]