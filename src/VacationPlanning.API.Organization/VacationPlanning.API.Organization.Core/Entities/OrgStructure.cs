namespace VacationPlanning.API.Organization.Core.Entities
{
    public class OrgStructure:
        BaseEntity
    {
        public string PositionName { get; set; }
        
        public Guid ParentId { get; set; }
        
        public int IsFolder { get; set; }
        
        public virtual  List<OrgStructure> Children { get; set; }

    }
}