namespace VacationPlanning.API.Organization.Core;

public class BaseEntity
{
    public Guid Id { get; set; }
}