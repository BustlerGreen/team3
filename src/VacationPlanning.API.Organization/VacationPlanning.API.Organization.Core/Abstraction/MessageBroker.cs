﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VacationPlanning.API.Organization.Core.Abstraction
{
    /// <summary>
    /// interface for using rabbit in DI 
    /// </summary>
    public interface IMessageBroker
    {
        void SendMessage(object obj, string key);
        void SendMessage(string message, string key);

    }

    
}
