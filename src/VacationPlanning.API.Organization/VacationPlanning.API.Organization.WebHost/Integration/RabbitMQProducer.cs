﻿using VacationPlanning.API.Organization.Core.Abstraction;
using RabbitMQ.Client;
using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Text;

namespace VacationPlanning.API.Organization.WebHost.Integration
{
    public class RabbitMQProducer : IMessageBroker, IDisposable
    {
        protected readonly ConnectionFactory _factory;
        protected readonly IConnection _connection;
        protected readonly IModel _channel;
        protected readonly RabbitMQSettings _settings;

        //---------------------------------------------------------------------------------------
        public RabbitMQProducer(IOptions<RabbitMQSettings> _option)
        {
            _settings = _option.Value;
            _factory = new ConnectionFactory() { HostName = _settings.HostName, 
                UserName = _settings.UserName, Password = _settings.Password };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchange: _settings.ExchangeName, type: "topic");
        }

        //---------------------------------------------------------------------------------------
        public void SendMessage(object obj, string key)
        {
            string so = JsonSerializer.Serialize(obj, typeof(object));
            SendMessage(so, key);
        }

        //---------------------------------------------------------------------------------------
        public void SendMessage(string message, string key)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(exchange: _settings.ExchangeName,
                routingKey: key,
                basicProperties: null,
                body: body);

        }

        //---------------------------------------------------------------------------------------
        public void Dispose()
        {
            _channel?.Dispose();
            _connection?.Dispose();

        }
    }
}
