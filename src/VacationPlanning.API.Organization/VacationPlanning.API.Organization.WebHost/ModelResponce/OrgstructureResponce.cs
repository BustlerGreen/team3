using VacationPlanning.API.Organization.Core.Entities;

namespace VacationPlanning.API.Organization.WebHost.ModelResponce;

public class OrgstructureResponce
{

    public Guid seed { get; set; }
    public ICollection<OrgStructure> orgStructureTree { get; set; }

}