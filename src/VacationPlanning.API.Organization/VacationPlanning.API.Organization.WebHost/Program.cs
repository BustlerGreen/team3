using System.Formats.Asn1;
using VacationPlanning.API.Organization.Core.Entities;
using VacationPlanning.API.Organization.Core.Interfaces;
using VacationPlanning.API.Organization.DataAccess;
using VacationPlanning.API.Organization.DataAccess.ModelConfiguration;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Microsoft.AspNetCore.Server.IISIntegration;
using VacationPlanning.API.Organization.WebHost.Integration;
using VacationPlanning.API.Organization.Core.Abstraction;
//using VacationPlanning.API.Organization.WebHost.Authentication;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Configuration
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", false, true)
//    //.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", false, true)
    .AddEnvironmentVariables();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddScoped(typeof(IRepositoryStructure<>), typeof(EfRepositoryStructure<>));

//Аутентификация
//builder.Services
 //   .AddAuthentication((IISDefaults.AuthenticationScheme))
  //  .AddNegotiate()

//.AddScheme<JwtSchemeOptions, JwtSchemeHandler(AuthSchemas.Jwt, options => { options.IsActive = true; });
    



// add message broker
builder.Services.Configure<RabbitMQSettings>(builder.Configuration.GetSection(nameof(RabbitMQSettings)));
builder.Services.AddSingleton<IMessageBroker, RabbitMQProducer>();


//БД с Оргструктурой
builder.Services.AddDbContext<DataContextStruct>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("Postgres"));
    
    /*options.UseNpgsql(builder.Configuration.GetConnectionString("PGStaffingConnect"),
                        b => b.MigrationsAssembly("ApplicationTier.API.OrganizationStructure"));
    */
    options.UseLazyLoadingProxies();
    
});


//builder.Configuration.AddEnvironmentVariables()
//     .AddUserSecrets(Assembly.GetExecutingAssembly(), true);

//для дат в PostgreSQL
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddSwaggerGen(c => {
    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
    c.IgnoreObsoleteActions();
    c.IgnoreObsoleteProperties();
    c.CustomSchemaIds(type => type.FullName);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    
}

app.UseAuthorization();

app.MapControllers();




//app.MapGet("/", (DataContextStruct db) => db.OrgStructures.ToList());

//Создание БД и загрузка начальных данных
using (var scope = app.Services.CreateScope())
{
    var dataContext = scope.ServiceProvider.GetRequiredService<DataContextStruct>();
    try
    {
        dataContext.Database.Migrate();
    }
    catch(Exception ex)
    { }
    var messBroker = scope.ServiceProvider.GetRequiredService<IMessageBroker>();
    messBroker.SendMessage("init", "init");
}

app.Run();






