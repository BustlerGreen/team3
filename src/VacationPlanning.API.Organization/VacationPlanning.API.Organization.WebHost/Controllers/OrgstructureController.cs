using VacationPlanning.API.Organization.WebHost.ModelResponce;
using VacationPlanning.API.Organization.Core.Entities;
using VacationPlanning.API.Organization.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using VacationPlanning.API.Organization.DataAccess;
using VacationPlanning.API.Organization.WebHost.Integration;
using VacationPlanning.API.Organization.Core.Abstraction;

namespace VacationPlanning.API.Organization.WebHost.Controllers;

/// <summary>
/// Оргштатная структура
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class OrgstructureController : ControllerBase
{
    private readonly IRepositoryStructure<OrgStructure> _orgstructureRepository;
    private readonly DataContextStruct _dataContext;
    private readonly IMessageBroker _messBroker;

    public OrgstructureController(IRepositoryStructure<OrgStructure> orgstructureRepository, DataContextStruct dataContext,
        IMessageBroker mb)
    {
        _orgstructureRepository = orgstructureRepository;
        _dataContext = dataContext;
        _messBroker = mb;
    }

    /// <summary>
    /// Получить всю оргструктуру
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<OrgStructure>> GetAllOrgsSructureAsync()
    {
        var orgStructure = await _orgstructureRepository.GetAllAsync();
        var orgstructureModelList = orgStructure.Select(x =>
            new OrgStructure()
            {
                Id = x.Id,
                PositionName = x.PositionName,
                ParentId = x.ParentId,
                IsFolder = x.IsFolder
            }).ToList();

        return orgstructureModelList;
    }

    /// <summary>
    /// Получить должность по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<OrgStructure>> GetPositionByIdAsync(Guid id)
    {
        var position = await _orgstructureRepository.GetByIdAsync(id);
        if (position == null)
        {
            return NotFound();
        }

        var positionModel = new OrgStructure()
        {
            Id = position.Id,
            ParentId = position.ParentId,
            PositionName = position.PositionName,
            IsFolder = position.IsFolder
        };
        return positionModel;
    }

    /// <summary>
    /// Создание должности
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreatePositionAsync(OrgStructure request)
    {
        OrgStructure position = new OrgStructure();
        position.PositionName = request.PositionName;
        position.ParentId = request.ParentId;
        position.IsFolder = request.IsFolder;

        await _orgstructureRepository.CreateAsync(position);
        _messBroker.SendMessage(position, "InsertPosition");

        return Ok();
    }

    /// <summary>
    /// Редактирование сотрудника
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditPositionAsync(Guid id, OrgStructure request)
    {
        var position = await _orgstructureRepository.GetByIdAsync(id);
        if (position == null)
        {
            return NotFound();
        }

        position.PositionName = request.PositionName;
        position.ParentId = request.ParentId;
        position.IsFolder = request.IsFolder;


        await _orgstructureRepository.UpdateAsync(position);
        _messBroker.SendMessage(position, "UpdatePosition");
        return Ok();
    }

    /// <summary>
    /// Получение подчиненных   по id начальника
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>    
    
    [HttpGet ]
    [Route("testold")]
    public   List<OrgStructure> GetOrgstructureTree(Guid id)
    {
        //полуаем всех соттрудников в лист чтобы не обращаться все время к БД
        List<OrgStructure> orgStructuresProxy = new List<OrgStructure>();
 
            
        List<OrgStructure> orgStructureProxy = _dataContext.OrgStructures.Where(x => x.Id !=null).ToList();    

        
        List<OrgStructure> lstOrgstructureWhithChilde =new List<OrgStructure>();

        //Получение все подчиненные должности    
        lstOrgstructureWhithChilde.AddRange(Helpers.Helper.GetOrgstructureTree(id, orgStructureProxy));

        return lstOrgstructureWhithChilde;
    }
}