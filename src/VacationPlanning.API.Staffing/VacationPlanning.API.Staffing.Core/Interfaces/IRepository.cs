namespace VacationPlanning.API.Staffing.Core.Interfaces
{
    public interface IRepository<T> 
    {
        public Task<IEnumerable<T>> GetAllAsync();

        public IQueryable<T> List();
        public Task<T> GetByIdAsync(Guid id);

        public Task<T> CreateAsync(T item);

        public Task<T> UpdateAsync(T item);
        
        public Task<T> DeleteAsync(T item);

        
    }
}