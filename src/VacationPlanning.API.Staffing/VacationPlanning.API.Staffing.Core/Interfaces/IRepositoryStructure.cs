namespace VacationPlanning.API.Staffing.Core.Interfaces
{
    public interface IRepositoryStructure<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<T> CreateAsync(T item);

        Task<T> UpdateAsync(T item);
        
        Task<T> DeleteAsync(T item);

        IQueryable<T> List();
    }
}    

