using System.Security.Cryptography.X509Certificates;

namespace VacationPlanning.API.Staffing.Core.Entities
{
    public class Holiday:
        BaseEntity
    {
        public  string HolidayName { get; set; }
        public DateTime DateStart { get; set; }
        
        public DateTime DateEnd { get; set; }
    }
}