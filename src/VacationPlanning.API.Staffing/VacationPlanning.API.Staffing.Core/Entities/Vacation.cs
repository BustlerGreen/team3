using System.Security.Cryptography.X509Certificates;

namespace VacationPlanning.API.Staffing.Core.Entities
{

    public class Vacation:
        BaseEntity
    {
        public Guid EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        
        public DateTime DateStart { get; set; }
        
        public  DateTime DateEnd { get; set; }
        
        //public Vacation Vacation { get; set; } ��� ��� ������ ����? ������ ������ �� ������ ���� ������� ������
        public Guid StatusId { get; set; }
        
        //public virtual Status Status { get; set; }
        
        // ������� ��������� ���� ��� ��� (1 - ���������, 0 - ��� )
        public  int Confirm { get; set; }
        
        // ������� ���� �� ����������� ���������� � �������������� ������� ��� ��� (1 - ��, 0 - ��� )
        public  int MailSend { get; set; }
        
        public string Comment { get; set; }
        public Guid VacationTypeId { get; set; }
//        public virtual VacationType VacationType  { get; set; }
    }
}