namespace VacationPlanning.API.Staffing.Core.Entities
{
/// <summary>
/// Состояние отпуска или командировки  ( может не понадобится )
/// </summary>
    public class Status:
        BaseEntity
    {
        public string StatusName { get; set; }
        //public IEnumerable<Vacation>? Vacation { get; set; }
    }
}