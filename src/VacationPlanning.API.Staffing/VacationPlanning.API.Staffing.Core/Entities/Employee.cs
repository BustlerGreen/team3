using System.ComponentModel.DataAnnotations.Schema;

namespace VacationPlanning.API.Staffing.Core.Entities
{
    
    /// <summary>
    /// Список сотрудников  
    /// </summary>
    public class Employee:
        BaseEntity
    {
        public  string Login { get; set; }
        
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        // Id сотрудника, замещающего на время отпуска
        public Guid ReplacementId { get; set; }

        
        public  Guid RoleId { get; set; }
        
        public virtual Role Role { get; set; }
        
        /// <summary>
        /// Юридическое лицо, в которой работает сотрудник
        /// </summary>        
        public Guid LegalEntityId  { get; set; }        
        
        /// Должность из класса OrgStructure
        /// </summary>
        public Guid OrgStructureId { get; set; }
        
        //    public virtual OrgStructure OrgStructure { get; set; }
        
        
        public virtual  ICollection<Vacation>  Vacation { get; set; }
        
        
    }
}