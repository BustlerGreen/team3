namespace VacationPlanning.API.Staffing.Core.Entities;

public class LegalEntity:
    BaseEntity
{
    public string LegalEntityName { get; set; }
    
    public Guid EmployeID { get; set; }
}