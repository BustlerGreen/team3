namespace VacationPlanning.API.Staffing.Core.Entities
{
     /// <summary>
    /// Документы  и их шаблоны для заполнения  
    /// </summary>
    public class Document:
        BaseEntity
    {
     public string DocumentName { get; set; }
     public string DocumentTemplate { get; set; }
     
     public  Guid VacationTypeId { get; set; }
     
     public  virtual VacationType VacationType { get; set; }
    }
}