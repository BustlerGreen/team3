﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;

namespace VacationPlanning.API.Staffing.Core.Integration
{
    /// <summary>
    /// Helper class
    /// Create or update DbSet<OrgStructure> by receiving message form message broker
    /// </summary>
    public interface IOrgStructUpdater
    {
        Task<bool> InsertOrgStruct(OrgStructure orgstr);
        Task<bool> UpdateOrgStruct(OrgStructure orgstr);
    }

    public class OrgStructUpdater : IOrgStructUpdater
    {
        private readonly IRepository<OrgStructure> _repo;

        public OrgStructUpdater(IRepository<OrgStructure> repo)
        {
            _repo = repo;
        }

        public async Task<bool> InsertOrgStruct(OrgStructure orgstr)
        {
            try
            {
                if (orgstr == null) return false;
               
               await _repo.CreateAsync(orgstr);
            }
            catch(Exception ex)
            {
                var i = 3;
            }
            return true;
        }

        public async Task<bool> UpdateOrgStruct(OrgStructure orgstr)
        {
            if (orgstr == null) return false;
            await _repo.UpdateAsync(orgstr);
            return true;
        }
    }
}
