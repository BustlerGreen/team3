﻿namespace VacationPlanning.API.Staffing.WebHost.Integration
{
    public class RabbitMQSettings
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ExchangeName { get; set; }
    }
}
