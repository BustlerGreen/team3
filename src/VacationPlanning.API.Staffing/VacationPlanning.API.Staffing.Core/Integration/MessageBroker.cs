﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VacationPlanning.API.Staffing.Core.Integration
{
    public interface IMessageBroker
    {
        void SendMessage(object obj, string key);
        void SendMessage(string message, string key);
    }
}
