﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VacationPlanning.API.Staffing.Core.Integration
{
    public class MailNotifyData
    {
        public string Body { get; set; }
        public string returnUrl { get; set; }
        public string mailTO { get; set; }

        public Guid id { get; set; }
    }
}
