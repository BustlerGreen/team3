using System.ComponentModel.DataAnnotations;
using VacationPlanning.API.Staffing.Core.Entities;

namespace VacationPlanning.API.Staffing.Core.Models
{

    public class LoginModel
    {

        [Required(ErrorMessage = "Отсутствует имя пользователя")]
        public string? Login { get; set; }

        [Required(ErrorMessage = "Отсутствует пароль")]
        [DataType(DataType.Password)]
        public string? Password { get; set; }
        
    }

}