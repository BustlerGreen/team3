namespace VacationPlanning.API.Staffing.Core;

public class BaseEntity
{
    public Guid Id { get; set; }
}