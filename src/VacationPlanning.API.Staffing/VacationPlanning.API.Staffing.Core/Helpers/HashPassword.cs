using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Text;

namespace VacationPlanning.API.Staffing.Core.Helpers;

public class HashPassword
{
    public static string ReturnHashOfPassword(string password)
    {
        string sSourceData = password;
        //Create a byte array from source data.
        byte[] tmpSource = ASCIIEncoding.ASCII.GetBytes(sSourceData);
        byte[] tmpHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);

        return ByteArrayToString(tmpHash);
    }
    
   
    
    private static string ByteArrayToString(byte[] arrInput)
    {
        int i;
        StringBuilder sOutput = new StringBuilder(arrInput.Length);
        for (i=0;i < arrInput.Length; i++)
        {
            sOutput.Append(arrInput[i].ToString("X2"));
        }
        return sOutput.ToString();
    }
   
    
    public static bool VerifyHashedPassword(string hashedPassword, string password)
    {
        byte[] buffer4;
        if (hashedPassword == null)
        {
            return false;
        }
        if (password == null)
        {
            throw new ArgumentNullException("password");
        }
        byte[] tmpNewHash = Convert.FromBase64String(hashedPassword);
        
        bool bEqual = false;
        if (tmpNewHash.Length == hashedPassword.Length)
        {
            int i=0;
            while ((i < tmpNewHash.Length) && (tmpNewHash[i] == password[i]))
            {
                i += 1;
            }
            if (i == tmpNewHash.Length)
            {
                bEqual = true;
            }
        }

        return bEqual;

    }
}
