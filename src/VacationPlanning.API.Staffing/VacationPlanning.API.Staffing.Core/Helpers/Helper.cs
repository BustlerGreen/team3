using VacationPlanning.API.Staffing.Core.Entities;

namespace VacationPlanning.API.Staffing.WebHost.Helpers;

public class Helper
{
    public static List<OrgStructure> GetOrgstructureTree(Guid id , List<OrgStructure> orgStructureProxy)
    {
 

        
        List<OrgStructure> lstOrgstructureWhithChilde = new List<OrgStructure>();

        //Приводим к классу OrgStructure, так как в листе получаются OrgstructureProxy
        List<OrgStructure> orgStructure = new List<OrgStructure>();
        foreach (var item in orgStructureProxy)
        {
            orgStructure.Add(new OrgStructure()
            {
                Id = item.Id,
                IsFolder = item.IsFolder,
                ParentId = item.ParentId,
                PositionName = item.PositionName
            });

        }

        //Добавляем верхний уровень ветки по которой ищем подчиненных 
        var position = orgStructure.Find(x => x.Id == id);
        if (position == null)
        {
            return null;
        }

        lstOrgstructureWhithChilde.Add(position);
        lstOrgstructureWhithChilde.AddRange(orgStructure.Where(x => x.ParentId == id).ToList());

        //Запускаем рекурсию для получения всех подчиненных
        foreach (var item in orgStructure.Where(x => x.ParentId == id).ToList())
        {
            lstOrgstructureWhithChilde.AddRange(Helpers.Helper.GetChildren(item.Id, orgStructure));
        }

        return lstOrgstructureWhithChilde;

    }

    //Рекурсивная функция для получения всех подчиненных в оргструктуре
    public static List<OrgStructure> GetChildren( Guid parentId, List<OrgStructure> lstOrtgStructure)
    {
        List<OrgStructure> lstChildren=new List<OrgStructure>();

        lstChildren.AddRange(lstOrtgStructure.Where(x => x.ParentId == parentId).ToList()); 
        
        foreach (var item in lstOrtgStructure.Where(x => x.ParentId == parentId).ToList())
        {
            lstChildren.AddRange(GetChildren(item.Id,lstOrtgStructure ));
        }
        return lstChildren;
    }
}