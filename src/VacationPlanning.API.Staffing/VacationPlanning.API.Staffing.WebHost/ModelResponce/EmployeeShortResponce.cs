using VacationPlanning.API.Staffing.Core.Entities;

namespace VacationPlanning.API.Staffing.WebHost.ModelResponce;

public class EmployeeShortResponce
{
    public Guid Id { get; set; }
        
    public string FullName { get; set; }

    public string Email { get; set; }
    
    public Role Role { get; set; }

    public Guid orgStructId { get; set; }
}