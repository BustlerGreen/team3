﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace VacationPlanning.API.Staffing.WebHost.ModelResponce
{
    public class CreateOrEditVacation
    {
        public Guid EmployeeId { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        //public Vacation Vacation { get; set; } что тут должно быть? ссылку класса на самого себя сделать нельзя
        public Guid StatusId { get; set; }

        //public virtual Status Status { get; set; }

        // признак утвержден план или нет (1 - утвержден, 0 - нет )
        public int Confirm { get; set; }

        // признак было ли уведомление сотруднику о приближающемся отпуске или нет (1 - да, 0 - нет )
        public int MailSend { get; set; }

        public string Comment { get; set; }
        public Guid VacationTypeId { get; set; }
    }
}
