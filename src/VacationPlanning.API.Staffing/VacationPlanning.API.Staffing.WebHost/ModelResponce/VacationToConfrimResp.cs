﻿namespace VacationPlanning.API.Staffing.WebHost.ModelResponce
{
    public class VacationToConfrimResp
    {
        public string fullName { get; set; }
        public Guid vacId { get; set; }

        public DateTime dateStart { get; set; }

        public DateTime dateEnd { get; set; }

        public string comment { get; set; }
    }
}
