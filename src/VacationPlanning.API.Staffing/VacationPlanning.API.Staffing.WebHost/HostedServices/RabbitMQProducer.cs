﻿using VacationPlanning.API.Staffing.Core.Integration;
using RabbitMQ.Client;
using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Text;
using VacationPlanning.API.Staffing.Core.Interfaces;
using VacationPlanning.API.Staffing.Core.Entities;
using System.Text.Json;

namespace VacationPlanning.API.Staffing.WebHost.Integration
{
    public class RabbitMQProducer : BackgroundService
    {
        protected readonly ConnectionFactory _factory;
        protected readonly IConnection _connection;
        protected readonly IModel _channel;
        protected readonly RabbitMQSettings _settings;
        private readonly IServiceProvider _serviceProvider;

        //---------------------------------------------------------------------------------------
        public RabbitMQProducer(IOptions<RabbitMQSettings> option, IServiceProvider sp)
        {
            _serviceProvider = sp;
            _settings = option.Value;
            _factory = new ConnectionFactory() { HostName = _settings.HostName,
                UserName = _settings.UserName, Password = _settings.Password };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchange: _settings.ExchangeName, type: "topic");
        }

        //---------------------------------------------------------------------------------------

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var notifyStartVacation = await getNewVacation();
                    foreach (MailNotifyData mnd in notifyStartVacation)
                    {
                        var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(mnd));
                        _channel.BasicPublish(exchange: _settings.ExchangeName,
                            routingKey: "ConfirmVacation",
                            basicProperties: null,
                            body: body);
                        List<Guid> vtu = new List<Guid>();
                        vtu.Add(mnd.id);
                        await SetMailSentFlag(vtu);
                    }

                    notifyStartVacation = await getVacationToBegin();
                    foreach (MailNotifyData mnd in notifyStartVacation)
                    {
                        var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(mnd));
                        _channel.BasicPublish(exchange: _settings.ExchangeName,
                            routingKey: "StartVacation",
                            basicProperties: null,
                            body: body);
                        List<Guid> vtu = new List<Guid>();
                        vtu.Add(mnd.id);
                        await SetMailSentFlag(vtu);
                    }
                }
                catch (Exception ex)
                {

                }
                await Task.Delay(30000);
            }
        }

        //---------------------------------------------------------------------------------------
        public override async Task StopAsync(CancellationToken ct)
        {
            await base.StopAsync(ct);
        }

        //---------------------------------------------------------------------------------------
        public override void Dispose()
        {
            _channel?.Dispose();
            _connection?.Dispose();
            base.Dispose();
        }


        protected async Task<List<MailNotifyData>> getNewVacation()
        {
            var scoped = _serviceProvider.CreateScope();
            {
                var _empRepo = scoped.ServiceProvider.GetRequiredService<IRepository<Employee>>();
                var _vacRepo = scoped.ServiceProvider.GetRequiredService<IRepository<Vacation>>();
                var _orgStrRepo = scoped.ServiceProvider.GetRequiredService<IRepository<OrgStructure>>();
                List<MailNotifyData> mnd = new List<MailNotifyData>();
                var vacs = _vacRepo.List().Where(v => v.Confirm == 0 && v.MailSend == 0).ToList();
                foreach (Vacation vac in vacs)
                {
                    var emp = await _empRepo.GetByIdAsync(vac.EmployeeId);
                    var targPos = await _orgStrRepo.GetByIdAsync(emp.OrgStructureId);
                    if (targPos.ParentId != Guid.Empty)
                    {
                        var targetEmpl = _empRepo.List().Where(e => e.OrgStructureId == targPos.ParentId).FirstOrDefault();
                        if (null != targetEmpl)
                        {
                            mnd.Add(new MailNotifyData
                            {
                                Body = targetEmpl.FirstName + ", Утвердите отпуск для сотрудника " + emp.FullName + 
                                    " Период отпуска " + vac.DateStart.ToString("dd.mm.yyyy") + " - " + vac.DateEnd.ToString("dd.mm.yyyy"),
                                mailTO = targetEmpl.Email,
                                returnUrl = "https://gitlab.com/BustlerGreen/team3",
                                id = vac.Id,
                            });
                        }
                    }
                };

                return mnd;
            }
        }

        protected async Task SetMailSentFlag(List<Guid> empIdList)
        {
            if(null == empIdList) return;
            var scoped = _serviceProvider.CreateScope();
            {
                var _vacRepo = scoped.ServiceProvider.GetRequiredService<IRepository<Vacation>>();
                foreach (Guid id in empIdList)
                {
                    if (id != Guid.Empty)
                    {
                        var vac = await _vacRepo.GetByIdAsync(id);
                        vac.MailSend = 1;
                        await _vacRepo.UpdateAsync(vac);
                    }
                }
            }
        }

        protected async Task<List<MailNotifyData>> getVacationToBegin()
        {
            var scoped = _serviceProvider.CreateScope();
            {
                var _empRepo = scoped.ServiceProvider.GetRequiredService<IRepository<Employee>>();
                var _vacRepo = scoped.ServiceProvider.GetRequiredService<IRepository<Vacation>>();
                var _orgStrRepo = scoped.ServiceProvider.GetRequiredService<IRepository<OrgStructure>>();
                List<MailNotifyData> mnd = new List<MailNotifyData>();
                var vacs = _vacRepo.List().Where(v => v.Confirm == 1 && v.MailSend == 0 
                && v.DateStart > DateTime.Now && v.DateStart < DateTime.Now.AddDays(14)).ToList();
                foreach (Vacation vac in vacs)
                {
                    var emp = await _empRepo.GetByIdAsync(vac.EmployeeId);
                    if (null != emp)
                    {
                        mnd.Add(new MailNotifyData
                        {
                            Body = emp.FullName + 
                                " У вас имеется запланированный отпуск,  " + vac.DateStart.ToString("dd.mm.yyyy") +
                                " - " + vac.DateEnd.ToString("dd.mm.yyyy"),
                            mailTO = emp.Email,
                            returnUrl = "https://gitlab.com/BustlerGreen/team3",
                            id = vac.Id,
                        });
                    }
                };

                return mnd;
            }
        }
    }
}
