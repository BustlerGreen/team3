﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using VacationPlanning.API.Staffing.WebHost.Integration;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Integration;

namespace VacationPlanning.API.Staffing.Core.HostedServices
{
    
    public class RabbitMQConsumer : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ConnectionFactory _factory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _queueName;
        private readonly RabbitMQSettings _settings;


        public RabbitMQConsumer(IOptions<RabbitMQSettings> option, IServiceProvider sp)
        {
            _settings = option.Value;
            _serviceProvider = sp;

            _factory = new ConnectionFactory() 
            { HostName= _settings.HostName, UserName= _settings.UserName, Password= _settings.Password };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _queueName = _channel.QueueDeclare().QueueName;
            try
            {
                _channel.QueueBind(queue: _queueName,
                    exchange: _settings.ExchangeName,
                    routingKey: "InsertPosition"
                    );
                _channel.QueueBind(queue: _queueName,
                    exchange: _settings.ExchangeName,
                    routingKey: "UpdatePosition"
                    );
            }
            catch (Exception ex)
            {

            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var consumer = new EventingBasicConsumer(_channel);
                    consumer.Received += async (model, ea) =>
                    {
                        string so = Encoding.UTF8.GetString(ea.Body.ToArray());
                        OrgStructure org = JsonSerializer.Deserialize<OrgStructure>(so);
                        using var scoped = _serviceProvider.CreateScope();
                        {
                            var scService = scoped.ServiceProvider.GetRequiredService<IOrgStructUpdater>();
                            switch (ea.RoutingKey)
                            {
                                case "InsertPosition":
                                    {
                                        await scService.InsertOrgStruct(org);
                                        break;
                                    }
                                case "UpdatePosition":
                                    {
                                        await scService.UpdateOrgStruct(org);
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                        }
                        _channel.BasicAck(ea.DeliveryTag, false);
                    };
                    _channel.BasicConsume(queue: _queueName, autoAck: false, consumer: consumer);
                }
                catch (Exception ex)
                {

                }
            }
        }
        public override async Task StopAsync(CancellationToken ct)
        {
            await base.StopAsync(ct);
        }

        public override void Dispose()
        {
            base.Dispose();
            _channel?.Dispose();
            _connection?.Dispose();
        }

    }
}
