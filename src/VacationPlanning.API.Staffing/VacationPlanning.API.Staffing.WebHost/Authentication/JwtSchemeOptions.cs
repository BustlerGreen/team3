using Microsoft.AspNetCore.Authentication;

namespace VacationPlanning.API.Staffing.WebHost.Authentication
{

    public class JwtSchemeOptions : AuthenticationSchemeOptions
    {
        public bool IsActive { get; set; }
    }

}