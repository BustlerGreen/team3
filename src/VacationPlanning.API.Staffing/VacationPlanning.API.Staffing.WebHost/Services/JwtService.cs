using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using VacationPlanning.API.Staffing.Core.Models;

namespace VacationPlanning.API.Staffing.WebHost.Services;

public class JwtService
{
    private static readonly string _secret = "my_super_secret_jwt";


    public static string GenerateRefreshToken()
    {
        return string.Empty;
    }

    public static string CreateJwtToken(List<Claim> authClaims)
    {
        var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_secret));
        var myIssuer = "MyCompany";
        var myAudience = "MyCompany";
        var tokenHandler = new JwtSecurityTokenHandler();
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(authClaims),
            Expires = DateTime.UtcNow.AddSeconds(30),
            Issuer = myIssuer,
            Audience = myAudience,
            SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}