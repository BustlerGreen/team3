using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// Статус оотпуска/командировки (дейситвующий, запланированный и т.п.)
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class StatusesController : ControllerBase
{
    private readonly IRepository<Status> _statusesRepository;


    public StatusesController(IRepository<Status> statusesRepository)
    {
        _statusesRepository = statusesRepository;
        

    }

    /// <summary>
        /// Получить все статусы
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Status>> GetAllStatusesAsync()
        {
            var statuses = await _statusesRepository.GetAllAsync();
            var statusesModelList = statuses.Select(x => 
                new Status()
                {
                    Id = x.Id,
                    StatusName = x.StatusName,

                }).ToList();

            return statusesModelList;
            
        }   
    
    /// <summary>
    /// Получить статус  по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Status>> GetStatusByIdAsync(Guid id)
    {
        var status = await _statusesRepository.GetByIdAsync(id);
        if (status == null)
        {
            return NotFound();
        }

        var statusModel = new Status()
        {
            Id = status.Id,
            StatusName = status.StatusName,
            
        };
        return statusModel;
    }
    
    /// <summary>
    /// Создание статуса
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateStatusAsync(Status request)
    {
        Status status = new Status();
        status.StatusName = request.StatusName;

        await _statusesRepository.CreateAsync(status);
        return Ok();
    }

    
    /// <summary>
    /// Редактирование статуса
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditStatusAsync(Guid id, Status request)
    {
        var status = await _statusesRepository.GetByIdAsync(id);
        if (status == null)
        {
            return NotFound();
        }

        status.StatusName = request.StatusName;

        await _statusesRepository.UpdateAsync(status);
        return Ok();
    }
    }
