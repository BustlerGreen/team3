using Microsoft.AspNetCore.Mvc;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Helpers;
using VacationPlanning.API.Staffing.Core.Interfaces;
using VacationPlanning.API.Staffing.Core.Models;
using VacationPlanning.API.Staffing.DataAccess;
using VacationPlanning.API.Staffing.WebHost.Services;
using System.Text.Json;
using System.Net.Http;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using System.Security.Claims;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;


[ApiController]
[Route("api/v1/[controller]")]
public class AuthController :  ControllerBase
{
    private readonly IRepository<Employee> _employeeRepository;
    private readonly DataContextStaffing _dataContext;
    
    public AuthController(IRepository<Employee> employeeRepository, DataContextStaffing dataContext)
    {
        _employeeRepository = employeeRepository;
         _dataContext = dataContext;
    }
    
  
    
    [HttpPost()]
    public JsonResult LoginNormal([FromBody] LoginModel loginModel)
    {
        //Находим пользователя по логину и паролю
        //EmployeesController.Get
        var hpass = HashPassword.ReturnHashOfPassword(loginModel.Password);
        var employeLogin = _dataContext.Employees.FirstOrDefault(
            x => x.Login == loginModel.Login && hpass ==  x.Password);

        if (null == employeLogin) return new JsonResult(new { token = "", refreshToken = "", 
            error = "invalid login/psw", role = "", id = "", fullName= "", orgId= "" });
        var authClaims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, employeLogin.Login),
            new Claim(ClaimTypes.Role, employeLogin.RoleId.ToString()),
        };
   
        var token = JwtService.CreateJwtToken(authClaims);

        var refreshToken = JwtService.GenerateRefreshToken();
        return new JsonResult(new { token = token, refreshToken="", error="", 
            role = employeLogin.RoleId, id = employeLogin.Id, fullName= employeLogin.FullName, orgId = employeLogin.OrgStructureId });

    }


}