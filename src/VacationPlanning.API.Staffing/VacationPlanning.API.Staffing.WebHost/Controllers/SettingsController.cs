using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// Документы (зваявления, приказы и т.п.)
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class SettingsController : ControllerBase
{
    private readonly IRepository<Setting> _settingsRepository;
    


    public SettingsController(IRepository<Setting> settingsRepository)
    {
        _settingsRepository = settingsRepository;
        

    }

    /// <summary>
        /// Получить все настройки
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Setting>> GetAllSettingsAsync()
        {
            var settings = await _settingsRepository.GetAllAsync();
            var settingsModelList = settings.Select(x => 
                new Setting()
                {
                    Id = x.Id,
                    SettingName = x.SettingName,
                    SettingType = x.SettingType,
                    SettingValue = x.SettingValue
                }).ToList();

            return settingsModelList;
            
        }   
    
    /// <summary>
    /// Получить настройку  по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Setting>> GetSettingByIdAsync(Guid id)
    {
        var setting = await _settingsRepository.GetByIdAsync(id);
        if (setting == null)
        {
            return NotFound();
        }

        var settingModel = new Setting()
        {
            Id = setting.Id,
            SettingName = setting.SettingName,
            SettingType = setting.SettingType,
            SettingValue = setting.SettingValue
            
            
        };
        return settingModel;
    }
    
    /// <summary>
    /// Создание настройки
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateSettingAsync(Setting request)
    {
        Setting setting = new Setting();
        setting.SettingName = request.SettingName;
        setting.SettingType = request.SettingType;
        setting.SettingValue = request.SettingValue;
        

        await _settingsRepository.CreateAsync(setting);
        return Ok();
    }

    
    /// <summary>
    /// Редактирование настройки
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditSettingAsync(Guid id, Setting request)
    {
        var setting = await _settingsRepository.GetByIdAsync(id);
        if (setting == null)
        {
            return NotFound();
        }

        setting.SettingName = request.SettingName;
        setting.SettingType = request.SettingType;
        setting.SettingValue = request.SettingValue;
        

        await _settingsRepository.UpdateAsync(setting);
        return Ok();
    }
    }
