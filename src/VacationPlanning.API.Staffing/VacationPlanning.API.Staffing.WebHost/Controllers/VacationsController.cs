using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using VacationPlanning.API.Staffing.DataAccess;
using Microsoft.AspNetCore.Authorization;
using VacationPlanning.API.Staffing.WebHost.Authentication;
using VacationPlanning.API.Staffing.WebHost.ModelResponce;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// Статус оотпуска/командировки (дейситвующий, запланированный и т.п.)
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class VacationsController : ControllerBase
{
    private readonly IRepository<Vacation> _vacationsRepository;
    private readonly IRepository<Employee> _employesRepository;
    private readonly DataContextStaffing _dataContext;


    public VacationsController(IRepository<Vacation> vacationsRepository, IRepository<Employee> employesRepository , DataContextStaffing dataContext)
    {
        _vacationsRepository = vacationsRepository;
        _employesRepository = employesRepository;
        _dataContext = dataContext;
        

    }

    /// <summary>
        /// Получить все отпуска
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Vacation>> GetAllVacationsAsync()
        {
            var vacations = await _vacationsRepository.GetAllAsync();
            var vacationsModelList = vacations.Select(x => 
                new Vacation()
                {
                    Id = x.Id,
                    EmployeeId= x.EmployeeId,
                    DateStart = x.DateStart,
                    DateEnd = x.DateEnd,
                    StatusId = x.StatusId,
                    Confirm = x.Confirm,
                    MailSend = x.MailSend,
                    Comment = x.Comment,
                    VacationTypeId = x.VacationTypeId

                }).ToList();

            return vacationsModelList;
            
        }   
    
    /// <summary>
    /// Получить отпуск  по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Vacation>> GetVacationByIdAsync(Guid id)
    {
        var vacation = await _vacationsRepository.GetByIdAsync(id);
        if (vacation == null)
        {
            return NotFound();
        }

        var vacationModel = new Vacation()
        {
            Id = vacation.Id,
            EmployeeId = vacation.EmployeeId,
            DateStart = vacation.DateStart,
            DateEnd = vacation.DateEnd,
            StatusId = vacation.StatusId,
            Comment = vacation.Comment,
            Confirm = vacation.Confirm,
            MailSend = vacation.MailSend,
            VacationTypeId = vacation.VacationTypeId
            
        };
        return vacationModel;
    }
    
    /// <summary>
    /// Создание отпуска
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateVacationAsync(ModelResponce.CreateOrEditVacation request)
    {
        Vacation vacation = new Vacation();
        vacation.EmployeeId = request.EmployeeId;
        vacation.DateStart = request.DateStart;
        vacation.DateEnd = request.DateEnd;
        vacation.StatusId = request.StatusId;
        vacation.Comment = request.Comment;
        vacation.VacationTypeId = request.VacationTypeId;
        vacation.Confirm = 0;
        vacation.MailSend = 0;

        await _vacationsRepository.CreateAsync(vacation);
        return Ok();
    }

    
    /// <summary>
    /// Редактирование отпуска
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditVacationAsync(Guid id, ModelResponce.CreateOrEditVacation request)
    {
        var vacation = await _vacationsRepository.GetByIdAsync(id);
        if (vacation == null)
        {
            return NotFound();
        }
      //Если план утвержден , то его редактировать нельзя
        if (vacation.Confirm != 1)
        {

            vacation.EmployeeId = request.EmployeeId;
            vacation.DateStart = request.DateStart;
            vacation.DateEnd = request.DateEnd;
            vacation.StatusId = request.StatusId;
            vacation.Comment = request.Comment;
            vacation.VacationTypeId = request.VacationTypeId;

            await _vacationsRepository.UpdateAsync(vacation);
            
        }
        return Ok();
        
    }

    /// <summary>
    /// Получить отпуска  по id сотрудника и году
    /// <param name="EmployeId"></param>
    /// <param name="year"></param>
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetVacationByEmployeID")]
    public async Task<List<Vacation>> GetVacationByEmployeIDAsync(Guid EmployeId, int year)
    {
        List<Vacation> vacations = _dataContext.Vacations.Where(x => x.EmployeeId == EmployeId && x.DateStart.Year == year).ToList();
        var vacationsModelList = vacations.Select(x => 
            new Vacation()
            {
                Id = x.Id,
                EmployeeId= x.EmployeeId,
                DateStart = x.DateStart,
                DateEnd = x.DateEnd,
                StatusId = x.StatusId,
                Comment = x.Comment,
                VacationTypeId = x.VacationTypeId,
                Confirm = x.Confirm,
                MailSend = x.MailSend

                
            }).ToList();
        return vacationsModelList;
    }

    /// <summary>
    /// Удаление отпуска по его ID
    /// <param name="VacationId"></param>
    /// </summary>
    /// <returns></returns>
    [HttpDelete("DeleteVacationByID")]
    [Authorize(AuthenticationSchemes = AuthSchemas.Jwt)]
    public async Task<IActionResult> DeleteVacationByID(Guid VacationId)
    {
        var vVacation = _dataContext.Vacations.FirstOrDefault(x => x.Id == VacationId);
        if (vVacation != null || vVacation.Confirm == 1)
        {
            _dataContext.Vacations.Remove(vVacation);
            await _dataContext.SaveChangesAsync();
        }

        return Ok();
    }



    /// <summary>
    /// Утверждение отпуска подчиненного по году и ID 
    /// </summary>
    /// <param name="EmployeId"></param>
    /// <param name="year"></param>
    /// /// <param name="confirm"></param>
    /// <returns></returns>
    [HttpPut("ConfirmVacationAsync")]
    public async Task<IActionResult> ConfirmVacationAsync(Guid VacationId)
    {

        //Апдейтим поле confirm у отпусков с выбранным сотрудникам

        var lVacation = await _vacationsRepository.GetByIdAsync(VacationId);
        if (lVacation == null) return NotFound();
        if (lVacation.Confirm == 0)
        {
            lVacation.Confirm = 1;
            lVacation.MailSend = 0;
            await _vacationsRepository.UpdateAsync(lVacation);
        }

        return Ok();

    }

    /// <summary>
    /// Получить отпуска  по id сотрудника и году
    /// <param name="EmployeId"></param>
    /// <param name="year"></param>
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetVacationToConfirm")]
    public List<VacationToConfrimResp> GetVacationToConfirmAsync(Guid orgId)
    {
        List<VacationToConfrimResp> resp = new List<VacationToConfrimResp>();
        List<OrgStructure> orgs = _dataContext.OrgStructures.Where(x => x.ParentId == orgId).ToList();
        foreach (OrgStructure org in orgs)
        {
            List<Employee> emps = _employesRepository.List().Where(x => x.OrgStructureId == org.Id).ToList();
            foreach (Employee emp in emps)
            {
                List<Vacation> vacs = _vacationsRepository.List().Where(x => x.EmployeeId == emp.Id && x.Confirm == 0).ToList();
                
                resp.AddRange(vacs.Select(x => new VacationToConfrimResp()
                {
                    fullName = emp.FullName,
                    dateEnd = x.DateEnd,
                    dateStart = x.DateStart,
                    comment = x.Comment,
                    vacId = x.Id
                }).ToList());
            }

        }
        return resp;
    }
}
