using VacationPlanning.API.Staffing.WebHost.ModelResponce;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using VacationPlanning.API.Staffing.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationPlanning.API.Staffing.Core.Helpers;
using Microsoft.AspNetCore.Authorization;
using  VacationPlanning.API.Staffing.WebHost.Authentication;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// Сотрудники
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class EmployeesController : ControllerBase
{
    private readonly IRepository<Employee> _employeeRepository;
    //private readonly DataContextStaffing _dataContext;


    public EmployeesController(IRepository<Employee> employeeRepository )
    {
        _employeeRepository = employeeRepository;
        //_dataContext = dataContext;
    }

    /// <summary>
    /// Получить всех сотрудников
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Authorize(AuthenticationSchemes=AuthSchemas.Jwt)]
    public async Task<List<EmployeeShortResponce>> GetAllEmployeeAsync()
    {
        var employees = await _employeeRepository.GetAllAsync();
        var employeesModelList = employees.Select(x => 
            new EmployeeShortResponce()
            {
                Id = x.Id,
                Email = x.Email,
                FullName = x.FullName
            }).ToList();

        return employeesModelList;
    }

    /// <summary>
    /// Получить данные сотрудника по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<EmployeeShortResponce>> GetEmployeeByIdAsync(Guid id)
    {
        var employee = await _employeeRepository.GetByIdAsync(id);
        if (employee == null)
        {
            return NotFound();
        }

        var employeeModel = new EmployeeShortResponce()
        {
            Id = employee.Id,
            FullName = employee.FullName,
            Email = employee.Email,
            Role = employee.Role,
            orgStructId = employee.OrgStructureId
        };
        return employeeModel;
    }

    /// <summary>
    /// Получить данные сотрудника по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetByOrg{id:guid}")]
    public List<EmployeeShortResponce> GetEmployeeByOrgAsync(Guid id)
    {
        var employee = _employeeRepository.List().Where(x=> x.OrgStructureId == id).ToList();
        
        var employeeList = employee.Select(x => new EmployeeShortResponce()
        {
            Id = x.Id,
            FullName = x.FullName,
            Email = x.Email,
            Role = x.Role,
            orgStructId = x.OrgStructureId
        }).ToList();
        return employeeList;
    }

    /// <summary>
    /// Создание сотрудника
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateEmployeeAsync(CreateOrEditEmployeerequest request)
    {
        Employee employee = new Employee();
        employee.Login = request.login;
        employee.Password = HashPassword.ReturnHashOfPassword(request.password);
        employee.FirstName = request.Firstname;
        employee.LastName = request.LastName;
        employee.Email = request.Enmail;
        employee.RoleId = request.RoleId;
        employee.ReplacementId = request.ReplacementId;
        await _employeeRepository.CreateAsync(employee);
        return Ok();
    }

/// <summary>
/// Редактирование сотрудника
/// </summary>
/// <param name="id"></param>
/// <param name="request"></param>
/// <returns></returns>
[HttpPut]
public async Task<IActionResult> EditEmployeeAsync(Guid id, CreateOrEditEmployeerequest request)
{
    var employee = await _employeeRepository.GetByIdAsync(id);
    if (employee == null)
    {
        return NotFound();
    }

    employee.Login = request.login;
    employee.Password = HashPassword.ReturnHashOfPassword(request.password);
    employee.FirstName = request.Firstname;
    employee.LastName = request.LastName;
    employee.Email = request.Enmail;
    employee.RoleId = request.RoleId;
    employee.ReplacementId = request.ReplacementId;

    await _employeeRepository.UpdateAsync(employee);
    return Ok();
}
    

}

