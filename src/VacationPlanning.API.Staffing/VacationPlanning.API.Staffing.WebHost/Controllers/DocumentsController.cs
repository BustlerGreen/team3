using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// Документы (зваявления, приказы и т.п.)
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class DocumentsController : ControllerBase
{
    private readonly IRepository<Document> _documentsRepository;
    //  private readonly DataContextStaffing _dataContext;


    public DocumentsController(IRepository<Document> documentRepository)
    {
        _documentsRepository = documentRepository;
        //    _dataContext = dataContext;

    }

    /// <summary>
        /// Получить все документы
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Document>> GetAllDocumentsAsync()
        {
            var documents = await _documentsRepository.GetAllAsync();
            var documentsModelList = documents.Select(x => 
                new Document()
                {
                    Id = x.Id,
                    DocumentName = x.DocumentName,
                    DocumentTemplate = x.DocumentTemplate
                }).ToList();

            return documentsModelList;
            
        }   
    
    /// <summary>
    /// Получить документ по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Document>> GetDocumentByIdAsync(Guid id)
    {
        var document = await _documentsRepository.GetByIdAsync(id);
        if (document == null)
        {
            return NotFound();
        }

        var documentModel = new Document()
        {
            Id = document.Id,
            DocumentName = document.DocumentName,
            DocumentTemplate = document.DocumentTemplate,
            VacationTypeId = document.VacationTypeId
            
            
        };
        return documentModel;
    }
    
    /// <summary>
    /// Создание документа
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateDocumentAsync(Document request)
    {
        Document document = new Document();
        document.DocumentName = request.DocumentName;
        document.DocumentTemplate = request.DocumentTemplate;
        document.VacationTypeId = request.VacationTypeId;
        

        await _documentsRepository.CreateAsync(document);
        return Ok();
    }

    
    /// <summary>
    /// Редактирование документа
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditDocumentAsync(Guid id, Document request)
    {
        var document = await _documentsRepository.GetByIdAsync(id);
        if (document == null)
        {
            return NotFound();
        }

        document.DocumentName = request.DocumentName;
        document.DocumentTemplate = request.DocumentTemplate;
        document.VacationTypeId = request.VacationTypeId;
        

        await _documentsRepository.UpdateAsync(document);
        return Ok();
    }
    }
