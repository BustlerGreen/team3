using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;


/// <summary>
/// Документы (зваявления, приказы и т.п.)
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class RolesController : ControllerBase
{
    private readonly IRepository<Role> _roleRepository;



    public RolesController(IRepository<Role> roleRepository)
    {
        _roleRepository = roleRepository;

    }

    /// <summary>
    /// Получить все роли
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<Role>> GetAllDocumentsAsync()
    {
        var roles = await _roleRepository.GetAllAsync();
        var rolesModelList = roles.Select(x => 
            new Role()
            {
                Id = x.Id,
                RoleName = x.RoleName,
                Description = x.Description
            }).ToList();

        return rolesModelList;
            
    }
    
    /// <summary>
    /// Получить роль по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Role>> GetDocumentByIdAsync(Guid id)
    {
        var role = await _roleRepository.GetByIdAsync(id);
        if (role == null)
        {
            return NotFound();
        }

        var roleModel = new Role()
        {
            Id = role.Id,
            RoleName = role.RoleName,
            Description = role.Description,

        };
        return roleModel;
    }
    
    /// <summary>
    /// Создание роли
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateRoletAsync(Role request)
    {
        Role role = new Role();
        role.RoleName = request.RoleName;
        role.Description = request.Description;
        
        

        await _roleRepository.CreateAsync(role);
        return Ok();
    }

    /// <summary>
    /// Редактирование роли
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditRoleAsync(Guid id, Role request)
    {
        var role = await _roleRepository.GetByIdAsync(id);
        if (role == null)
        {
            return NotFound();
        }

        role.RoleName = request.RoleName;
        role.Description = request.Description;
        
        

        await _roleRepository.UpdateAsync(role);
        return Ok();
    }
    
}