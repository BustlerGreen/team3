using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using VacationPlanning.API.Staffing.DataAccess;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// Сотрудники
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class HolydaysController : ControllerBase
{
    private readonly IRepository<Holiday> _holydayRepository;
    //private readonly DataContextStaffing _dataContext;

    public HolydaysController(IRepository<Holiday> holydayRepository )
    {
        _holydayRepository = holydayRepository;
        //_dataContext = dataContext;
    }

    /// <summary>
    /// Получить все праздники
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<Holiday>> GetAllHolidaysAsync()
    {
        var holidays = await _holydayRepository.GetAllAsync();
        var holydaysModelList = holidays.Select(x => 
            new Holiday()
            {
                Id = x.Id,
                HolidayName = x.HolidayName,
                DateStart = x.DateStart,
                DateEnd = x.DateEnd,
                
            }).ToList();

        return holydaysModelList;
    }    
    
    /// <summary>
    /// Получить праздник по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Holiday>> GetHolidayByIdAsync(Guid id)
    {
        var holiday = await _holydayRepository.GetByIdAsync(id);
        if (holiday == null)
        {
            return NotFound();
        }

        var holidayModel = new Holiday()
        {
            Id = holiday.Id,
            HolidayName = holiday.HolidayName,
            DateStart = holiday.DateStart,
            DateEnd = holiday.DateEnd
        };
        return holidayModel;
    }
    
    /// <summary>
    /// Создание праздника
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateHolidayAsync( Holiday request)
    {
        Holiday holiday = new Holiday();
        holiday.HolidayName = request.HolidayName;
        holiday.DateStart = request.DateStart;
        holiday.DateEnd = request.DateEnd;
        

        await _holydayRepository.CreateAsync(holiday);
        return Ok();
    }
    
    /// <summary>
    /// Редактирование праздника
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditHollidayAsync(Guid id, Holiday request)
    {
        var holiday = await _holydayRepository.GetByIdAsync(id);
        if (holiday == null)
        {
            return NotFound();
        }

        holiday.HolidayName = request.HolidayName;
        holiday.DateStart = request.DateStart;
        holiday.DateEnd = request.DateEnd;
        

        await _holydayRepository.UpdateAsync(holiday);
        return Ok();
    }
}