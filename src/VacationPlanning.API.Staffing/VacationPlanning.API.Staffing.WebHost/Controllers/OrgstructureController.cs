
using Microsoft.AspNetCore.Mvc;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using VacationPlanning.API.Staffing.DataAccess;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;
[ApiController]
[Route("api/v1/[controller]")]
public class OrgstructureController : ControllerBase
{
    private readonly IRepository<OrgStructure> _orgstructureRepository;
    private readonly DataContextStaffing _dataContext;
    
    public OrgstructureController(IRepository<OrgStructure> orgstructureRepository, DataContextStaffing dataContext)
    {
        _orgstructureRepository = orgstructureRepository;
        _dataContext = dataContext;
    }
    
    /// <summary>
    /// Получить всю оргструктуру
    /// </summary>
    /// <returns></returns>

    [HttpGet]
    public async Task<List<OrgStructure>> GetAllOrgsSructureAsync()
    {
        var orgStructure = await _orgstructureRepository.GetAllAsync();
        var orgstructureModelList = orgStructure.Select(x =>
            new OrgStructure()
            {
                Id = x.Id,
                PositionName = x.PositionName,
                ParentId = x.ParentId,
                IsFolder = x.IsFolder
            }).ToList();

        return orgstructureModelList;
    }
        /// <summary>
    /// Получить должность по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<OrgStructure>> GetPositionByIdAsync(Guid id)
    {
        var position = await _orgstructureRepository.GetByIdAsync(id);
        if (position == null)
        {
            return NotFound();
        }

        var positionModel = new OrgStructure()
        {
            Id = position.Id,
            ParentId = position.ParentId,
            PositionName = position.PositionName,
            IsFolder = position.IsFolder
        };
        return positionModel;
    }

    /// <summary>
    /// Создание должности
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreatePositionAsync(OrgStructure request)
    {
        OrgStructure position = new OrgStructure();
        position.PositionName = request.PositionName;
        position.ParentId = request.ParentId;
        position.IsFolder = request.IsFolder;

        await _orgstructureRepository.CreateAsync(position);
        return Ok();
    }

    /// <summary>
    /// Редактирование сотрудника
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditPositionAsync(Guid id, OrgStructure request)
    {
        var position = await _orgstructureRepository.GetByIdAsync(id);
        if (position == null)
        {
            return NotFound();
        }

        position.PositionName = request.PositionName;
        position.ParentId = request.ParentId;
        position.IsFolder = request.IsFolder;


        await _orgstructureRepository.UpdateAsync(position);
        return Ok();
    }
    /// <summary>
    /// Получение подчиненных должностей   по id начальника 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>    
    
    [HttpGet("test")]
    public  List<OrgStructure> GetOrgstructureTree(Guid id)
    {
        //полуаем всех соттрудников в лист чтобы не обращаться все время к БД
        List<OrgStructure> orgStructuresProxy =  _dataContext.OrgStructures.Where(x => x.Id !=Guid.Empty).ToList();;
 
        
        List<OrgStructure> lstOrgstructureWhithChilde =new List<OrgStructure>();
        //Получение все подчиненные должности    
        lstOrgstructureWhithChilde .AddRange(Helpers.Helper.GetOrgstructureTree(id, orgStructuresProxy));

        return lstOrgstructureWhithChilde;
    }

    [HttpGet("GetTopChilds")]
    public List<OrgStructure> GetTopChilds(Guid id)
    {
        //полуаем всех соттрудников в лист чтобы не обращаться все время к БД
        List<OrgStructure> res = _dataContext.OrgStructures.Where(x => x.ParentId == id).ToList(); ;

        return res;
    }


}