using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace VacationPlanning.API.Staffing.WebHost.Controllers;

/// <summary>
/// тип оотпуска/командировки 
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class VacationTypesController : ControllerBase
{
    private readonly IRepository<VacationType> _vacationTypesRepository;
    


    public VacationTypesController(IRepository<VacationType> vacationTypesRepository)
    {
        _vacationTypesRepository = vacationTypesRepository;
        

    }

    /// <summary>
        /// Получить все типы отпусков (отпуск, командировка и т.п.)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<VacationType>> GetAllVacationTypesAsync()
        {
            var vacationTypes = await _vacationTypesRepository.GetAllAsync();
            var vacationTypesModelList = vacationTypes.Select(x => 
                new VacationType()
                {
                    Id = x.Id,
                    VacationName = x.VacationName
                    
                }).ToList();

            return vacationTypesModelList;
            
        }   
    
    /// <summary>
    /// Получить тип отпуска  по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<VacationType>> GetVacationTypeByIdAsync(Guid id)
    {
        var vacationType = await _vacationTypesRepository.GetByIdAsync(id);
        if (vacationType == null)
        {
            return NotFound();
        }

        var vacationTypeModel = new VacationType()
        {
            Id = vacationType.Id,
            VacationName = vacationType.VacationName

        };
        return vacationTypeModel;
    }
    
    /// <summary>
    /// Создание типа отпуска
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateVacationTypeAsync(VacationType request)
    {
        VacationType vacationType = new VacationType();
        vacationType.VacationName = request.VacationName;

        await _vacationTypesRepository.CreateAsync(vacationType);
        return Ok();
    }

    
    /// <summary>
    /// Редактирование типа отпуска
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> EditVacationTypeAsync(Guid id, VacationType request)
    {
        var vacationType = await _vacationTypesRepository.GetByIdAsync(id);
        if (vacationType == null)
        {
            return NotFound();
        }

        vacationType.VacationName = request.VacationName;
        
        await _vacationTypesRepository.UpdateAsync(vacationType);
        return Ok();
    }
    }
