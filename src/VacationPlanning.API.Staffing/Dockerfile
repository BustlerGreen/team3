#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR "/src"
COPY ["VacationPlanning.API.Staffing.WebHost/VacationPlanning.API.Staffing.WebHost.csproj", "VacationPlanning.API.Staffing.WebHost/"]
COPY ["VacationPlanning.API.Staffing.Core/VacationPlanning.API.Staffing.Core.csproj", "VacationPlanning.API.Staffing.Core/"]
COPY ["VacationPlanning.API.Staffing.DataAccess/VacationPlanning.API.Staffing.DataAccess.csproj", "VacationPlanning.API.Staffing.DataAccess/"]
RUN dotnet restore "VacationPlanning.API.Staffing.WebHost/VacationPlanning.API.Staffing.WebHost.csproj"
COPY . .
WORKDIR "/src/VacationPlanning.API.Staffing.WebHost"
RUN dotnet build "VacationPlanning.API.Staffing.WebHost.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "VacationPlanning.API.Staffing.WebHost.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "VacationPlanning.API.Staffing.WebHost.dll"]