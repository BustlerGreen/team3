﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VacationPlanning.API.Staffing.DataAccess.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Holidays",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    HolidayName = table.Column<string>(type: "text", nullable: false),
                    DateStart = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    DateEnd = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Holidays", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LegalEntities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    LegalEntityName = table.Column<string>(type: "text", nullable: false),
                    EmployeID = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalEntities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrgStructures",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PositionName = table.Column<string>(type: "text", nullable: false),
                    ParentId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsFolder = table.Column<int>(type: "integer", nullable: false),
                    OrgStructureId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrgStructures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrgStructures_OrgStructures_OrgStructureId",
                        column: x => x.OrgStructureId,
                        principalTable: "OrgStructures",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleName = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SettingName = table.Column<string>(type: "text", nullable: false),
                    SettingType = table.Column<string>(type: "text", nullable: false),
                    SettingValue = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Status",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    StatusName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Status", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VacationTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    VacationName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VacationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Login = table.Column<string>(type: "text", nullable: false),
                    Password = table.Column<string>(type: "text", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: false),
                    LastName = table.Column<string>(type: "text", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: false),
                    ReplacementId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    LegalEntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    OrgStructureId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    DocumentName = table.Column<string>(type: "text", nullable: false),
                    DocumentTemplate = table.Column<string>(type: "text", nullable: false),
                    VacationTypeId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documents_VacationTypes_VacationTypeId",
                        column: x => x.VacationTypeId,
                        principalTable: "VacationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vacations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    EmployeeId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateStart = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    DateEnd = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    StatusId = table.Column<Guid>(type: "uuid", nullable: false),
                    Confirm = table.Column<int>(type: "integer", nullable: false),
                    MailSend = table.Column<int>(type: "integer", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: false),
                    VacationTypeId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vacations_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vacations_VacationTypes_VacationTypeId",
                        column: x => x.VacationTypeId,
                        principalTable: "VacationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Holidays",
                columns: new[] { "Id", "DateEnd", "DateStart", "HolidayName" },
                values: new object[] { new Guid("53729686-a369-4eeb-8bfa-cc69b6051d03"), new DateTime(2022, 5, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Тестовый праздник" });

            migrationBuilder.InsertData(
                table: "LegalEntities",
                columns: new[] { "Id", "EmployeID", "LegalEntityName" },
                values: new object[] { new Guid("11729686-a368-4eeb-8bfa-cc69b6050d02"), new Guid("53729686-a368-4eeb-8bfa-cc69b6050d03"), "Рога и копыта" });

            migrationBuilder.InsertData(
                table: "OrgStructures",
                columns: new[] { "Id", "IsFolder", "OrgStructureId", "ParentId", "PositionName" },
                values: new object[,]
                {
                    { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), 1, null, new Guid("00000000-0000-0000-0000-000000000000"), "Руководитель" },
                    { new Guid("63729686-a368-4eeb-8bfa-cc69b6050d02"), 1, null, new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Начальник IT" },
                    { new Guid("68729686-a368-4eeb-8bfa-cc69b6050d02"), 2, null, new Guid("63729686-a368-4eeb-8bfa-cc69b6050d02"), "Программист" },
                    { new Guid("73729686-a368-4eeb-8bfa-cc69b6050d02"), 1, null, new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Начальник ChR" },
                    { new Guid("73739686-a368-4eeb-8bfa-cc69b6050d02"), 2, null, new Guid("73729686-a368-4eeb-8bfa-cc69b6050d02"), "Сотрудник ChR" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "RoleName" },
                values: new object[,]
                {
                    { new Guid("53739686-a368-4eeb-8bfa-cc69b6050d03"), "СисАдмин", "Администратор" },
                    { new Guid("53749686-a368-4eeb-8bfa-cc69b6050d03"), "Сотрудник HR", "HR" },
                    { new Guid("53759686-a368-4eeb-8bfa-cc69b6050d03"), "Обычный Сотрудник", "Сотрудник" }
                });

            migrationBuilder.InsertData(
                table: "Status",
                columns: new[] { "Id", "StatusName" },
                values: new object[,]
                {
                    { new Guid("58729686-a369-4eeb-8bfa-cc69b6051d03"), "План" },
                    { new Guid("58729786-a369-4eeb-8bfa-cc69b6051d03"), "Текущий отмпуск" },
                    { new Guid("58789786-a369-4eeb-8bfa-cc69b6051d03"), "Использованный отпуск" }
                });

            migrationBuilder.InsertData(
                table: "VacationTypes",
                columns: new[] { "Id", "VacationName" },
                values: new object[,]
                {
                    { new Guid("93729686-a369-4eeb-8bfa-cc69b6051d03"), "Отпуск" },
                    { new Guid("95729686-a369-4eeb-8bfa-cc69b6051d03"), "Командировка" }
                });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "DocumentName", "DocumentTemplate", "VacationTypeId" },
                values: new object[,]
                {
                    { new Guid("53729686-a369-4eeb-8bfa-cc69b6051d03"), "Заявление на отпуск", "Statement.doc", new Guid("93729686-a369-4eeb-8bfa-cc69b6051d03") },
                    { new Guid("63729686-a369-4eeb-8bfa-cc69b6051d03"), "Заявление на перенос отпуска", "ReplaceStatement.doc", new Guid("93729686-a369-4eeb-8bfa-cc69b6051d03") }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Email", "FirstName", "LastName", "LegalEntityId", "Login", "OrgStructureId", "Password", "ReplacementId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("33729686-a369-4eeb-8bfa-cc69b6050d03"), "133@com.ru", "Василий", "Семенов", new Guid("11729686-a368-4eeb-8bfa-cc69b6050d02"), "444", new Guid("63729686-a368-4eeb-8bfa-cc69b6050d02"), "550A141F12DE6341FBA65B0AD0433500", new Guid("53829686-a368-4eeb-8bfa-cc69b6050d03"), new Guid("53759686-a368-4eeb-8bfa-cc69b6050d03") },
                    { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d03"), "121@com.ru", "Игорь", "Петров", new Guid("11729686-a368-4eeb-8bfa-cc69b6050d02"), "111", new Guid("68729686-a368-4eeb-8bfa-cc69b6050d02"), "698D51A19D8A121CE581499D7B701668", new Guid("53829686-a368-4eeb-8bfa-cc69b6050d03"), new Guid("53739686-a368-4eeb-8bfa-cc69b6050d03") },
                    { new Guid("53729686-a369-4eeb-8bfa-cc69b6050d03"), "133@com.ru", "Семен", "Кудрин", new Guid("11729686-a368-4eeb-8bfa-cc69b6050d02"), "333", new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "310DCBBF4CCE62F762A2AAA148D556BD", new Guid("53829686-a368-4eeb-8bfa-cc69b6050d03"), new Guid("53759686-a368-4eeb-8bfa-cc69b6050d03") },
                    { new Guid("53829686-a368-4eeb-8bfa-cc69b6050d03"), "1213@com.ru", "Сергей", "Иванов", new Guid("11729686-a368-4eeb-8bfa-cc69b6050d02"), "222", new Guid("73729686-a368-4eeb-8bfa-cc69b6050d02"), "BCBE3365E6AC95EA2C0343A2395834DD", new Guid("53729686-a368-4eeb-8bfa-cc69b6050d03"), new Guid("53749686-a368-4eeb-8bfa-cc69b6050d03") }
                });

            migrationBuilder.InsertData(
                table: "Vacations",
                columns: new[] { "Id", "Comment", "Confirm", "DateEnd", "DateStart", "EmployeeId", "MailSend", "StatusId", "VacationTypeId" },
                values: new object[] { new Guid("53729686-a369-4eeb-8bfa-cc69b6051d03"), "Отпуск по графику", 1, new DateTime(2022, 1, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("53729686-a368-4eeb-8bfa-cc69b6050d03"), 1, new Guid("58729686-a369-4eeb-8bfa-cc69b6051d03"), new Guid("93729686-a369-4eeb-8bfa-cc69b6051d03") });

            migrationBuilder.CreateIndex(
                name: "IX_Documents_VacationTypeId",
                table: "Documents",
                column: "VacationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_RoleId",
                table: "Employees",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_OrgStructures_OrgStructureId",
                table: "OrgStructures",
                column: "OrgStructureId");

            migrationBuilder.CreateIndex(
                name: "IX_Vacations_EmployeeId",
                table: "Vacations",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Vacations_VacationTypeId",
                table: "Vacations",
                column: "VacationTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "Holidays");

            migrationBuilder.DropTable(
                name: "LegalEntities");

            migrationBuilder.DropTable(
                name: "OrgStructures");

            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.DropTable(
                name: "Status");

            migrationBuilder.DropTable(
                name: "Vacations");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "VacationTypes");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
