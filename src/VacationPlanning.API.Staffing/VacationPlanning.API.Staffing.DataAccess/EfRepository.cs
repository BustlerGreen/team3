using VacationPlanning.API.Staffing.Core;
using VacationPlanning.API.Staffing.Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace VacationPlanning.API.Staffing.DataAccess
{
    public class EfRepository<T>: IRepository<T> 
        where T:BaseEntity
    {
        private readonly DataContextStaffing _dataContext;
        public EfRepository(DataContextStaffing dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var res = await _dataContext.Set<T>().ToListAsync();
            return res;
        }

        
        public async Task<T> GetByIdAsync(Guid id)
        {
            var res =  await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return res;
        }

        public async Task<T> CreateAsync(T item)
        {
            var res = await _dataContext.Set<T>().AddAsync(item);
            await _dataContext.SaveChangesAsync();
            return res.Entity;
          
        }

        public async Task<T> UpdateAsync(T entity)
        {
            _dataContext.Set<T>().Update(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

        public Task<T> DeleteAsync(T item)
        {
            _dataContext.Set<T>().Remove(item);
            _dataContext.SaveChanges();
            return Task.FromResult(item);
        }

        public IQueryable<T> List()
        {
            return _dataContext.Set<T>();
        }
    }
}