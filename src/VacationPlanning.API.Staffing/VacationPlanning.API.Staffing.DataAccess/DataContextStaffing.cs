using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using VacationPlanning.API.Staffing.Core.Entities;
using VacationPlanning.API.Staffing.Core.Helpers;

namespace VacationPlanning.API.Staffing.DataAccess;

public class DataContextStaffing : DbContext
{
    public DbSet<Document> Documents { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<Holiday> Holidays { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<Setting> Settings { get; set; }
    public DbSet<Vacation> Vacations { get; set; }
    public DbSet<VacationType> VacationTypes { get; set; }
    public DbSet<OrgStructure> OrgStructures { get; set; }
    public DbSet<LegalEntity> LegalEntities { get; set; }

    private IConfiguration _config;

    public DataContextStaffing(DbContextOptions<DataContextStaffing> options, IConfiguration config)
        : base(options)
    {
       
        _config = config;

    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        
       // optionsBuilder.UseNpgsql(_config.GetConnectionString("Postgres"));
        //for migration
       optionsBuilder.UseNpgsql("Host=localhost;Port=5434;Database=PgDbStaff;Username=vacplan;Password=vacplan");
        
       
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

        //Роли
        modelBuilder.Entity<Role>().HasData(
            new Role()
            {
                Id = Guid.Parse("53739686-a368-4eeb-8bfa-cc69b6050d03"),
                RoleName = "Администратор",
                Description = "СисАдмин"
            },
            new Role()
            {
                Id = Guid.Parse("53749686-a368-4eeb-8bfa-cc69b6050d03"),
                RoleName = "HR",
                Description = "Сотрудник HR"
            },

            new Role()
            {
                Id = Guid.Parse("53759686-a368-4eeb-8bfa-cc69b6050d03"),
                RoleName = "Сотрудник",
                Description = "Обычный Сотрудник"
            }
        );


        //Сотрудники
        modelBuilder.Entity<Employee>().HasData(
            new Employee()
            {

                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d03"),
                Password = HashPassword.ReturnHashOfPassword("111"),
                Login = "111",
                FirstName = "Игорь",
                LastName = "Петров",
                Email = "121@com.ru",
                ReplacementId = Guid.Parse("53829686-a368-4eeb-8bfa-cc69b6050d03"),
                RoleId = Guid.Parse("53739686-a368-4eeb-8bfa-cc69b6050d03"),
                OrgStructureId = Guid.Parse("68729686-a368-4eeb-8bfa-cc69b6050d02"),
                LegalEntityId = Guid.Parse("11729686-a368-4eeb-8bfa-cc69b6050d02")

            },
            new Employee()
            {
                Id = Guid.Parse("53829686-a368-4eeb-8bfa-cc69b6050d03"),
                Password = HashPassword.ReturnHashOfPassword("222"),
                Login = "222",
                FirstName = "Сергей",
                LastName = "Иванов",
                Email = "1213@com.ru",
                ReplacementId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d03"),
                RoleId = Guid.Parse("53749686-a368-4eeb-8bfa-cc69b6050d03"),
                OrgStructureId = Guid.Parse("73729686-a368-4eeb-8bfa-cc69b6050d02"),
                //Role = Roles.FirstOrDefault(x=>x.RoleName=="Администратор")
                LegalEntityId = Guid.Parse("11729686-a368-4eeb-8bfa-cc69b6050d02")
            },

            new Employee()
            {
                Id = Guid.Parse("53729686-a369-4eeb-8bfa-cc69b6050d03"),
                Password = HashPassword.ReturnHashOfPassword("333"),
                Login = "333",
                FirstName = "Семен",
                LastName = "Кудрин",
                Email = "133@com.ru",
                ReplacementId = Guid.Parse("53829686-a368-4eeb-8bfa-cc69b6050d03"),
                RoleId = Guid.Parse("53759686-a368-4eeb-8bfa-cc69b6050d03"),
                OrgStructureId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                LegalEntityId = Guid.Parse("11729686-a368-4eeb-8bfa-cc69b6050d02")

            },
             new Employee()
             {
                 Id = Guid.Parse("33729686-a369-4eeb-8bfa-cc69b6050d03"),
                 Password = HashPassword.ReturnHashOfPassword("444"),
                 Login = "444",
                 FirstName = "Василий",
                 LastName = "Семенов",
                 Email = "133@com.ru",
                 ReplacementId = Guid.Parse("53829686-a368-4eeb-8bfa-cc69b6050d03"),
                 RoleId = Guid.Parse("53759686-a368-4eeb-8bfa-cc69b6050d03"),
                 OrgStructureId = Guid.Parse("63729686-a368-4eeb-8bfa-cc69b6050d02"),
                 LegalEntityId = Guid.Parse("11729686-a368-4eeb-8bfa-cc69b6050d02")

             }
        );

        //Документы
        modelBuilder.Entity<Document>().HasData(
        new Document()
        {
            Id = Guid.Parse("53729686-a369-4eeb-8bfa-cc69b6051d03"),
            DocumentName = "Заявление на отпуск",
            DocumentTemplate = "Statement.doc",
            VacationTypeId = Guid.Parse("93729686-a369-4eeb-8bfa-cc69b6051d03")
        },
        new Document()
        {
            Id = Guid.Parse("63729686-a369-4eeb-8bfa-cc69b6051d03"),
            DocumentName = "Заявление на перенос отпуска",
            DocumentTemplate = "ReplaceStatement.doc",
            VacationTypeId = Guid.Parse("93729686-a369-4eeb-8bfa-cc69b6051d03")

        }
     );

        //Праздники
        modelBuilder.Entity<Holiday>().HasData(
            new Holiday()
            {
                Id = Guid.Parse("53729686-a369-4eeb-8bfa-cc69b6051d03"),
                HolidayName = "Тестовый праздник",
                DateStart = DateTime.ParseExact("01/05/2022 00:00:00", "dd/MM/yyyy HH:mm:ss", null),
                DateEnd = DateTime.ParseExact("10/05/2022 00:00:00", "dd/MM/yyyy HH:mm:ss", null)
            }

        );

        modelBuilder.Entity<Status>().HasData(
            new Status()
            {
                Id = Guid.Parse("58729686-a369-4eeb-8bfa-cc69b6051d03"),
                StatusName = "План"
            },
            new Status()
            {
                Id = Guid.Parse("58729786-a369-4eeb-8bfa-cc69b6051d03"),
                StatusName = "Текущий отмпуск"
            },
            new Status()
            {
                Id = Guid.Parse("58789786-a369-4eeb-8bfa-cc69b6051d03"),
                StatusName = "Использованный отпуск"
            }

        );

        //Отпуски
        modelBuilder.Entity<Vacation>().HasData(
            new Vacation()
            {
                Id = Guid.Parse("53729686-a369-4eeb-8bfa-cc69b6051d03"),
                EmployeeId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d03"),
                VacationTypeId = Guid.Parse("93729686-a369-4eeb-8bfa-cc69b6051d03"),
                Comment = "Отпуск по графику",
                StatusId = Guid.Parse("58729686-a369-4eeb-8bfa-cc69b6051d03"),
                DateStart = DateTime.ParseExact("20/01/2022 00:00:00", "dd/MM/yyyy HH:mm:ss", null),
                DateEnd = DateTime.ParseExact("30/01/2022 00:00:00", "dd/MM/yyyy HH:mm:ss", null),
                Confirm = 1,
                MailSend = 1
            }

        );
        //Тип отпуска
        modelBuilder.Entity<VacationType>().HasData(
            new VacationType()
            {
                Id = Guid.Parse("93729686-a369-4eeb-8bfa-cc69b6051d03"),
                VacationName = "Отпуск"
            },
        new VacationType()
        {
            Id = Guid.Parse("95729686-a369-4eeb-8bfa-cc69b6051d03"),
            VacationName = "Командировка"
        }
        );
        //Добавлены дублирующие таблицы оргструктуры и юр. лиц для микросервисов
        modelBuilder.Entity<OrgStructure>().HasData(
            new OrgStructure()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Руководитель",
                //ParentId = null,
                IsFolder = 1

            },
            new OrgStructure()
            {
                Id = Guid.Parse("63729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Начальник IT",
                ParentId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 1
            },
            new OrgStructure()
            {
                Id = Guid.Parse("68729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Программист",
                ParentId = Guid.Parse("63729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 2
            },
            new OrgStructure()
            {
                Id = Guid.Parse("73729686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Начальник ChR",
                ParentId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 1
            },
            new OrgStructure()
            {
                Id = Guid.Parse("73739686-a368-4eeb-8bfa-cc69b6050d02"),
                PositionName = "Сотрудник ChR",
                ParentId = Guid.Parse("73729686-a368-4eeb-8bfa-cc69b6050d02"),
                IsFolder = 2
            }

        );
        //Добавление юр лица
        modelBuilder.Entity<LegalEntity>().HasData(
            new LegalEntity()
            {
                Id = Guid.Parse("11729686-a368-4eeb-8bfa-cc69b6050d02"),
                LegalEntityName = "Рога и копыта",
                EmployeID = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d03")


            }
        );
    }

    //=====================================================================================================
    public class AppDbContextFactory : IDesignTimeDbContextFactory<DataContextStaffing>
    {
        public DataContextStaffing CreateDbContext(string[] args)
        {
            var OptionsBuilder = new DbContextOptionsBuilder<DataContextStaffing>();
            OptionsBuilder.UseNpgsql("Host=localhost;Port=5434;Database=PgDbStaff;Username=vacplan;Password=vacplan");
            return new DataContextStaffing(OptionsBuilder.Options, null);
        }
    }
}



