﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IOrgstructureService
    {
        Task<IEnumerable<OrgStructure>> GetItems();

        Task<HttpResponseMessage> Post(OrgStructure localData);

        Task<HttpResponseMessage> Put(Guid id, OrgStructure localData);

        Task<OrgStructure> Get(Guid id);
    }
}
