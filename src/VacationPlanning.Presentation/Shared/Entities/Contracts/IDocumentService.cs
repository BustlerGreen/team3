﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    public interface IDocumentService
    {

        Task<IEnumerable<Document>> GetItems();

        Task<HttpResponseMessage> Post(Document document);

        Task<HttpResponseMessage> Put(Guid id, Document document);

        Task<Document> Get(Guid id);


    }
}
