﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IHolydaysService
    {
        Task<IEnumerable<Holiday>> GetItems();

        Task<HttpResponseMessage> Post(Holiday localData);

        Task<HttpResponseMessage> Put(Guid id, Holiday localData);

        Task<Holiday> Get(Guid id);
    }
}
