﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IVacationsService
    {
        Task<IEnumerable<Vacation>> GetItems();

        Task<HttpResponseMessage> Post(Vacation localData);

        Task<HttpResponseMessage> Put(Guid id, Vacation localData);

        Task<Vacation> Get(Guid id);
    }
}
