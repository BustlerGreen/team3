﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IVacationTypesService
    {
        Task<IEnumerable<VacationType>> GetItems();

        Task<HttpResponseMessage> Post(VacationType localData);

        Task<HttpResponseMessage> Put(Guid id, VacationType localData);

        Task<VacationType> Get(Guid id);
    }
}
