﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface ILegalEntityService
    {

        Task<IEnumerable<LegalEntity>> GetItems();

        Task<HttpResponseMessage> Post(LegalEntity localData);

        Task<HttpResponseMessage> Put(Guid id, LegalEntity localData);

        Task<LegalEntity> Get(Guid id);
    }
}
