﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface ISettingsService
    {
        Task<IEnumerable<Setting>> GetItems();

        Task<HttpResponseMessage> Post(Setting localData);

        Task<HttpResponseMessage> Put(Guid id, Setting localData);

        Task<Setting> Get(Guid id);
    }
}
