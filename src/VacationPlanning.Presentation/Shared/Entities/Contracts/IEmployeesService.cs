﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IEmployeesService
    {
        Task<IEnumerable<Employee>> GetItems();

        Task<HttpResponseMessage> Post(Employee localData);

        Task<HttpResponseMessage> Put(Guid id, Employee localData);

        Task<Employee> Get(Guid id);
    }
}
