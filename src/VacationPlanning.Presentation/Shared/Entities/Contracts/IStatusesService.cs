﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IStatusesService
    {
        Task<IEnumerable<Status>> GetItems();

        Task<HttpResponseMessage> Post(Status localData);

        Task<HttpResponseMessage> Put(Guid id, Status localData);

        Task<Status> Get(Guid id);
    }
}
