﻿using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Contracts
{
    internal interface IRolesService
    {
        Task<IEnumerable<Role>> GetItems();

        Task<HttpResponseMessage> Post(Role localData);

        Task<HttpResponseMessage> Put(Guid id, Role localData);

        Task<Role> Get(Guid id);
    }
}
