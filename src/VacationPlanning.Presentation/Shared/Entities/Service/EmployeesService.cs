﻿using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using System.Net.Http.Json;

using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Service
{
    internal class EmployeesService : IEmployeesService
    {
        private readonly HttpClient _httpClient;
        private readonly string APILocal = "/api/v1/Employees";

        public EmployeesService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<Employee> Get(Guid id)
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<Employee>($"{APILocal}/{id}");

                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<Employee>> GetItems()
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<IEnumerable<Employee>>(APILocal);


                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }
        }

        public async Task<HttpResponseMessage> Post(Employee localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(APILocal, localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Put(Guid id, Employee localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{APILocal}?id={id}", localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
