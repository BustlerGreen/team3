﻿using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using System.Net.Http.Json;

using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Service
{
    internal class HolydaysService : IHolydaysService
    {


        private readonly HttpClient _httpClient;
        private readonly string APILocal = "/api/v1/Holydays";

        public HolydaysService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Holiday> Get(Guid id)
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<Holiday>($"{APILocal}/{id}");

                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<Holiday>> GetItems()
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<IEnumerable<Holiday>>(APILocal);


                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }
        }

        public async Task<HttpResponseMessage> Post(Holiday localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(APILocal, localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Put(Guid id, Holiday localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{APILocal}?id={id}", localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
