﻿using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using System.Net.Http.Json;

using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Service
{
    internal class StatusesService : IStatusesService
    {

        private readonly HttpClient _httpClient;
        private readonly string APILocal = "/api/v1/Statuses";

        public StatusesService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Status> Get(Guid id)
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<Status>($"{APILocal}/{id}");

                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<Status>> GetItems()
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<IEnumerable<Status>>(APILocal);


                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }
        }

        public async Task<HttpResponseMessage> Post(Status localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(APILocal, localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Put(Guid id, Status localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{APILocal}?id={id}", localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
