﻿using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using System.Net.Http.Json;

using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Service
{
    internal class OrgstructureService : IOrgstructureService
    {

        private readonly HttpClient _httpClient;
        private readonly string APILocal = "/api/v1/Orgstructure";

        public OrgstructureService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<OrgStructure> Get(Guid id)
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<OrgStructure>($"{APILocal}/{id}");

                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<OrgStructure>> GetItems()
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<IEnumerable<OrgStructure>>(APILocal);


                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }
        }

        public async Task<HttpResponseMessage> Post(OrgStructure localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(APILocal, localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Put(Guid id, OrgStructure localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{APILocal}?id={id}", localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
