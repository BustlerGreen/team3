﻿using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using System.Net.Http.Json;

using VacationPlanning.API.Staffing.Core.Entities;
namespace PresentationTier.BlazorUI.Shared.Entities.Service
{
    public class DocumentService : IDocumentService
    {
        private readonly HttpClient _httpClient;
        private readonly string APILocal = "/api/v1/Documents";

        public DocumentService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Document>> GetItems()
        {
            try
            {

                var doc = await _httpClient.GetFromJsonAsync<IEnumerable<Document>>(APILocal);


                return doc;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }
        }

        public async Task<Document> Get(Guid id)
        {
            try
            {

                var doc = await _httpClient.GetFromJsonAsync<Document>($"{APILocal}/{id}");

                return doc;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Post(Document document)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(APILocal, document);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Put(Guid id, Document document)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{APILocal}?id={id}", document);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
