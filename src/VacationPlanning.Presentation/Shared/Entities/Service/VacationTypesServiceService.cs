﻿using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using System.Net.Http.Json;

using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Shared.Entities.Service
{
    internal class VacationTypesServiceService : IVacationTypesService
    {

        private readonly HttpClient _httpClient;
        private readonly string APILocal = "/api/v1/VacationTypes";


        public VacationTypesServiceService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<VacationType> Get(Guid id)
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<VacationType>($"{APILocal}/{id}");

                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<VacationType>> GetItems()
        {
            try
            {

                var localData = await _httpClient.GetFromJsonAsync<IEnumerable<VacationType>>(APILocal);


                return localData;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }
        }

        public async Task<HttpResponseMessage> Post(VacationType localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(APILocal, localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<HttpResponseMessage> Put(Guid id, VacationType localData)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{APILocal}?id={id}", localData);

                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
