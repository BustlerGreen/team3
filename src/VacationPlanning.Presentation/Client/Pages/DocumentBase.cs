﻿using Microsoft.AspNetCore.Components;

using PresentationTier.BlazorUI.Shared.Entities.Contracts;

using VacationPlanning.API.Staffing.Core.Entities;

namespace PresentationTier.BlazorUI.Client.Pages
{
    public class DocumentBase : ComponentBase
    {
        [Inject]
        public IDocumentService DocumentService { get; set; }

        public IEnumerable<Document> Documents { get; set; }


        protected override async Task OnInitializedAsync()
        {
            this.Documents = await DocumentService.GetItems();
        }


    }
}
