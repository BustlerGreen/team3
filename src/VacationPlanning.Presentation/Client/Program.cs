using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

using PresentationTier.BlazorUI.Client;
using PresentationTier.BlazorUI.Shared.Entities.Contracts;
using PresentationTier.BlazorUI.Shared.Entities.Service;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("http://localhost:5228/") });


builder.Services.AddScoped<IDocumentService, DocumentService>();

await builder.Build().RunAsync();
