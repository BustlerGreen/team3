﻿namespace VacationPlanning.MailNotifier.Template.Emails.ConfirmVacationCreateOrder;

public record ConfirmVacationCreateOrderViewModel(string ConfirmGuid, string body); // add url later with guid param