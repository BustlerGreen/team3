﻿namespace VacationPlanning.MailNotifier.Template.Shared;

public record EmailButtonViewModel(string Text, string Url);