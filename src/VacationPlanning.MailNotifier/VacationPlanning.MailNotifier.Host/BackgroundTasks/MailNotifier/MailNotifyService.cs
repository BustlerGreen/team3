﻿
using Microsoft.Extensions.Options;
using MimeKit;
using MailKit.Net.Smtp;

namespace VacationPlanning.MailNotifier.Host.BackgroundTasks.MailNotifier
{
    /// <summary>
    /// mail notify interface
    /// </summary>
    public interface IMailNotifyService
    {
        public Task<bool> SendAsync(MailData mailData, string body,CancellationToken ct);
    }

    /// <summary>
    /// IMailNotifyService implementation 
    /// </summary>
    public class MailNotifyService : IMailNotifyService
    {
        private readonly MailSettings _mailSettings;
        private readonly ILogger<MailNotifyService>? _logger;
        
        private readonly IServiceScopeFactory _serviceScopeFactory;


        //----------------------------------------------------------------------------------------------
        public MailNotifyService(IOptions<MailSettings> settings, ILogger<IMailNotifyService> logger, 
            IServiceScopeFactory scopefactory)
        {
            _mailSettings = settings.Value;
            _logger = _logger;
            
            _serviceScopeFactory = scopefactory;
        }

        //----------------------------------------------------------------------------------------------
        public async Task<bool> SendAsync(MailData mailData, string body, CancellationToken ct = default)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                try
                {
                    var mail = new MimeMessage();
                    //sender
                    mail.From.Add(new MailboxAddress(_mailSettings.DisplayName, mailData.From ?? _mailSettings.From));
                    mail.Sender = new MailboxAddress(mailData.DisplayName ?? _mailSettings.DisplayName,
                        mailData.From ?? _mailSettings.From);
                    //receiver
                    foreach (string mailAddress in mailData.To) mail.To.Add(MailboxAddress.Parse(mailAddress));
                    if (!string.IsNullOrEmpty(mailData.ReplayTo))
                        mail.ReplyTo.Add(new MailboxAddress(mailData.ReplayTo, mailData.ReplayTo));
                    mail.Subject = mailData.Subject ?? "Default Subject";
                                       
                    //content of message
                    mail.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                    {
                        Text = body
                    };

                    var smtp = new SmtpClient();
                    smtp.CheckCertificateRevocation = false;
                    if (_mailSettings.UseSSL)
                        await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port,
                            MailKit.Security.SecureSocketOptions.SslOnConnect, ct);
                    else if (_mailSettings.UseStartTls)
                        await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port,
                            MailKit.Security.SecureSocketOptions.StartTls, ct); 
                    else
                        await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port,
                            MailKit.Security.SecureSocketOptions.None, ct);
                    await smtp.AuthenticateAsync(_mailSettings.UserName, _mailSettings.Password, ct);
                    await smtp.SendAsync(mail, ct);
                    await smtp.DisconnectAsync(true, ct);

                    return true;
                }
                catch (Exception ex)
                {
                    _logger?.LogError("send message failed {0}", ex);
                    return false;
                }
            }
        }
    }
}
