﻿namespace VacationPlanning.MailNotifier.Host.BackgroundTasks.MailNotifier
{
    public class MailData
    {
        public string? From { get; set; }
        public List<string>? To { get; set; }
        public List<string>? Cc { get; set; }
        public string? DisplayName { get; set; }
        public string? ReplayTo { get; set; }
        public string Subject { get; set; }
        public string? Body { get; set; }
    }
}
