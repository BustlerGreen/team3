﻿namespace VacationPlanning.MailNotifier.Host.BackgroundTasks.MailNotifier

{
    public class RabbitMQSettings
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ExchangeName { get; set; }
    }

    public class MailNotifyData
    {
        public string Body { get; set; }
        public string returnUrl { get; set; }
        public string mailTO { get; set; }

        public Guid id { get; set; }
    }
}
