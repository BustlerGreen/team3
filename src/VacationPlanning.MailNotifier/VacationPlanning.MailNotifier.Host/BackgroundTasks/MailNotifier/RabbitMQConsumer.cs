﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using VacationPlanning.MailNotifier.Template.Emails.ConfirmVacationCreateOrder;
using VacationPlanning.MailNotifier.Template.Services;

namespace VacationPlanning.MailNotifier.Host.BackgroundTasks.MailNotifier
{
    
    public class RabbitMQConsumer : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ConnectionFactory _factory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _queueName;
        private readonly RabbitMQSettings _settings;


        public RabbitMQConsumer(IOptions<RabbitMQSettings> option, IServiceProvider sp)
        {
            _settings = option.Value;
            _serviceProvider = sp;

            _factory = new ConnectionFactory() 
            { HostName= _settings.HostName, UserName= _settings.UserName, Password= _settings.Password };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _queueName = _channel.QueueDeclare().QueueName;
            try
            {
                _channel.QueueBind(queue: _queueName,
                    exchange: _settings.ExchangeName,
                    routingKey: "ConfirmVacation"
                    );
                _channel.QueueBind(queue: _queueName,
                    exchange: _settings.ExchangeName,
                    routingKey: "StartVacation"
                    );
            }
            catch (Exception ex)
            {

            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var consumer = new EventingBasicConsumer(_channel);
                    consumer.Received += async (model, ea) =>
                    {
                        string so = Encoding.UTF8.GetString(ea.Body.ToArray());
                        MailNotifyData? mnd = JsonSerializer.Deserialize<MailNotifyData>(so);
                        using var scoped = _serviceProvider.CreateScope();
                        {
                            var scService = scoped.ServiceProvider.GetRequiredService<IMailNotifyService>();
                            var scopedRender = scoped.ServiceProvider.GetRequiredService<IRazorViewToStringRenderer>();
                            switch (ea.RoutingKey)
                            {
                                case "ConfirmVacation":
                                    {
                                        MailData md = new MailData()
                                        {
                                            To = new List<string>() { mnd.mailTO, },
                                            Body = mnd.Body,
                                            Subject = "Подверждение заявки на отпуск",
                                        };
                                        var confirmVacationCreateOrderModel = new ConfirmVacationCreateOrderViewModel(mnd.returnUrl, md.Body);
                                        var body = await scopedRender.RenderViewToStringAsync(
                                                    "/Views/Emails/ConfirmVacationCreateOrder/ConfirmVacationCreateOrder.cshtml", confirmVacationCreateOrderModel);
                                        await scService.SendAsync(md, body, stoppingToken);
                                        break;
                                    }
                                case "UpdatePosition":
                                    {
                                        MailData md = new MailData()
                                        {
                                            To = new List<string>() { mnd.mailTO, },
                                            Body = mnd.Body,
                                            Subject = "Очередной отпуск",
                                        };
                                        var confirmVacationCreateOrderModel = new ConfirmVacationCreateOrderViewModel("https://gitlab.com/BustlerGreen/team3/-/tree/AShevchenko", md.Body);
                                        var body = await scopedRender.RenderViewToStringAsync(
                                                    "/Views/Emails/ConfirmVacationCreateOrder/ConfirmVacationCreateOrder.cshtml", confirmVacationCreateOrderModel);
                                        await scService.SendAsync(md, body, stoppingToken);
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }
                        }
                        _channel.BasicAck(ea.DeliveryTag, false);
                    };
                    _channel.BasicConsume(queue: _queueName, autoAck: false, consumer: consumer);
                    await Task.Delay(500);

                }
                catch (Exception ex)
                {

                }
            }
        }

        
        public override async Task StopAsync(CancellationToken ct)
        {
            await base.StopAsync(ct);
        }

        public override void Dispose()
        {
            base.Dispose();
            _channel?.Dispose();
            _connection?.Dispose();
        }

    }
}
