﻿using VacationPlanning.MailNotifier.Template.Services;
using VacationPlanning.MailNotifier.Template.Emails.ConfirmVacationCreateOrder;
using VacationPlanning.MailNotifier.Host.BackgroundTasks.MailNotifier;

namespace VacationPlanning.MailNotifier.Host.BackgroundTask.MailNotifier
{
    /// <summary>
    /// Using scoped service in singleton according to
    /// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-3.1&tabs=visual-studio#consuming-a-scoped-service-in-a-background-task
    /// </summary>
    internal interface IMailNotifyProcessingService
    {
        Task NotifyIfAny(CancellationToken ct);
    }

    //========================================================================================================
    public class MailNotifyHostedService : BackgroundService
    {
        private readonly ILogger<MailNotifyHostedService> _logger;
        public IServiceProvider Services { get; }
        public MailNotifyHostedService(IServiceProvider services, ILogger<MailNotifyHostedService> logger)
        {
            Services = services;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken ct)
        {
            await ScopedDoMailNotify(ct);
        }

        private async Task ScopedDoMailNotify(CancellationToken ct)
        {
            using (var scope = Services.CreateScope())
            {
                var scopedMailNotifyService = scope.ServiceProvider.GetRequiredService<IMailNotifyProcessingService>();
                await scopedMailNotifyService.NotifyIfAny(ct);
            }
        }

        public override async Task StopAsync(CancellationToken ct)
        {
            await base.StopAsync(ct);
        }
    }

    /// <summary>
    /// check if there are tasks to notify (check DataBase)
    /// </summary>
    //========================================================================================================
    public class MailNotifyProcessing : IMailNotifyProcessingService
    {
        private readonly ILogger<MailNotifyProcessing> _logger;
        private readonly IMailNotifyService _mailNotifyService;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        //----------------------------------------------------------------------------------------------
        public MailNotifyProcessing(ILogger<MailNotifyProcessing> logger, IMailNotifyService mailNotifier,
            IRazorViewToStringRenderer razorViewToStringRenderer)
        {
            _logger = logger;
            _mailNotifyService = mailNotifier;
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        
        //----------------------------------------------------------------------------------------------
        public  async Task NotifyIfAny(CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
            {
                try
                {
                    var nl = await GetPersonsToNotifyStartVacation();
                    foreach (NotifyRec nr in nl)
                    {
                        MailData mailData = new MailData();
                        mailData.To = new List<string>();
                        mailData.To.AddRange(nr.To);
                        mailData.Subject = nr.Subject;

                        await _mailNotifyService.SendAsync(mailData, nr.body, new CancellationToken());
                    }
                }
                catch (Exception ex)
                {

                }
                await Task.Delay(TimeSpan.FromSeconds(5));
            }

        }

        protected struct NotifyRec
        {
            public List<string> To;
            public string body;
            public string Subject;
        }

        //----------------------------------------------------------------------------------------------
        protected async Task<List<NotifyRec>> GetPersonsToNotifyStartVacation()
        {
            List<NotifyRec> nrl = new List<NotifyRec>();
            NotifyRec nr = new NotifyRec();
            nr.To = new List<string>();
            nr.To.AddRange(new List<string>(){
                "LitvinovVN@rambler.ru", 
                "ilitar@yahoo.com",
                "smikalian@gmail.com",
                "azarayaos@gmail.com",
                "Mikhail.Shchepetkin@slk-cement.com"
            });
            nr.Subject = "Your vacation is about to begin";
            string vacDates = $"{DateTime.Now.AddDays(-14).ToString("dd.MM.yyyy")} - {DateTime.Now.ToString("dd.MM.yyyy")}";
            var confirmVacationCreateOrderModel = new ConfirmVacationCreateOrderViewModel("https://gitlab.com/BustlerGreen/team3/-/tree/AShevchenko", vacDates);
            nr.body = await _razorViewToStringRenderer.RenderViewToStringAsync(
                        "/Views/Emails/ConfirmVacationCreateOrder/ConfirmVacationCreateOrder.cshtml", confirmVacationCreateOrderModel);
            nrl.Add(nr);
            return nrl;
        }
        
        
    }
}
