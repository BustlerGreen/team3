using Microsoft.EntityFrameworkCore;
using VacationPlanning.MailNotifier.Host.BackgroundTasks.MailNotifier;
using VacationPlanning.MailNotifier.Template.Services;
using System.Reflection;
using VacationPlanning.MailNotifier.Host.Mongo;
using VacationPlanning.MailNotifier.Host.Abstractions;
using VacationPlanning.MailNotifier.Host.Repositories;


var builder = WebApplication.CreateBuilder(args);


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Configuration
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", false, true)
    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", false, true)
    .AddEnvironmentVariables();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//Mail notifier settings
builder.Services.Configure<MailSettings>(builder.Configuration.GetSection(nameof(MailSettings)));
builder.Services.AddScoped<IMailNotifyService, MailNotifyService>();
//builder.Services.AddHostedService<MailNotifyHostedService>();
//builder.Services.AddScoped<IMailNotifyProcessingService, MailNotifyProcessing>();
builder.Services.Configure<RabbitMQSettings>(builder.Configuration.GetSection(nameof(RabbitMQSettings)));
builder.Services.AddHostedService<RabbitMQConsumer>();
builder.Services.AddRazorPages();
builder.Services.AddScoped<IRazorViewToStringRenderer, RazorViewToStringRenderer>();

builder.Services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));
builder.Services.Configure<MongoSettings>(builder.Configuration.GetSection(nameof(MongoSettings)));


builder.Configuration.AddEnvironmentVariables()
     .AddUserSecrets(Assembly.GetExecutingAssembly(), true);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    
}

app.UseAuthorization();

app.MapControllers();


app.Run();






