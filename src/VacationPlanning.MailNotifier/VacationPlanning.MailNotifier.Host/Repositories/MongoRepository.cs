﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Linq.Expressions;
using VacationPlanning.MailNotifier.Host.Abstractions;
using VacationPlanning.MailNotifier.Host.Mongo;

namespace VacationPlanning.MailNotifier.Host.Repositories
{
    public  class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _mongoCollection;

        //----------------------------------------------------------------------------------------
        public MongoRepository(IOptions<MongoSettings> option )
        {
            _mongoCollection = new MongoClient(option.Value.ConnectionString)
                .GetDatabase(option.Value.DatabaseName)
                .GetCollection<T>(typeof(T).Name.ToString());
        }

        //----------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var res = await _mongoCollection.Find(_ => true).ToListAsync();
            return res;
        }

        //----------------------------------------------------------------------------------------
        public async Task<T> GetByIdAsync(Guid id)
        {
            var res = await _mongoCollection.Find(rec => rec.Id == id).FirstOrDefaultAsync();
            return res;
        }

        //----------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var res = await _mongoCollection.Find(rec => ids.Contains(rec.Id)).ToListAsync();
            return res;
        }

        //----------------------------------------------------------------------------------------
        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var res = await _mongoCollection.Find(predicate).FirstOrDefaultAsync();
            return res;
        }

        //----------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var res = await _mongoCollection.Find(predicate).ToListAsync();
           
            return res;
        }

        //----------------------------------------------------------------------------------------
        public async Task AddAsync(T entity)
        {
            await _mongoCollection.InsertOneAsync(entity);
        }

        //----------------------------------------------------------------------------------------
        public async Task UpdateAsync(T entity)
        {
            await _mongoCollection.ReplaceOneAsync(rec => rec.Id == entity.Id, entity);
        }

        //----------------------------------------------------------------------------------------
        public async Task DeleteAsync(T entity)
        {
            await _mongoCollection.DeleteOneAsync(rec => rec.Id == entity.Id);

        }
    }
}
