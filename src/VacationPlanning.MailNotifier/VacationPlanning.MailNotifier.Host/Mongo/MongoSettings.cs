﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationPlanning.MailNotifier.Host.Mongo

{
    public  class MongoSettings
    {
        public string ConnectionString { get; set; } = null;
        public string DatabaseName { get; set; } = null;
    }
}
