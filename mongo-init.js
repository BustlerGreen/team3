db.auth("pmadmin", "pmadmin");
db = db.getSiblingDB("PromoCode");
db.log.insertOne({"message": "Database created."});
db.createUser(
	{
	    user: "vacplan",
	    pwd: "vacplan",
	    roles: [
		{
		    role: "readWrite",
                    db: "vacplanDocs"
		}
	    ]
	}
);